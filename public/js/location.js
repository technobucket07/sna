
var Url = window.location.origin+'/';

$(document).on("change","#country",{target : $("#locations")},fetchLocations);
function fetchLocations(event)
{
    var countryId = $(this).val();
    var locationInput = event.data.target;
    var URL = window.location.origin+'/';
    var options = '<option></option>';
    $.ajax({
        url : URL+"locations/byCountry/"+countryId,
        type: 'get',
        success : function(response){
            var locations = JSON.parse(response);
            $.each(locations,function(id,val){
                options += "<option value='"+id+"'>"+val+"</option>";
            })
        },
        complete: function(){
            locationInput.html(options);
            locationInput.select2();
        }

    });
}