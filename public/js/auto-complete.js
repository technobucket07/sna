
/* Admin Autocomplete --------------------------------------- */

function autocomplete(tablename,fieldname,fieldid,hiddenfieldid,url){

  $("."+fieldname).autocomplete({
        source: url+"/admin/autocomplete/"+tablename+"/"+fieldname+"/"+fieldid,// path to method
         select: function ( event, ui ) {

           if (fieldid !=null) {


              var id= ui.item.id;
               if(hiddenfieldid != ''){
                   $("."+hiddenfieldid).val(id);

               }

           }

       }
    })
}

function autocompleteRedirect(tablename,fieldname,fieldid,hiddenfieldid,url){

  $("."+fieldname).autocomplete({
        source: url+"/admin/autocomplete/"+tablename+"/"+fieldname+"/"+fieldid,// path to method
         select: function ( event, ui ) {
           if (fieldid !=null) {

              var id= ui.item.id;
               if(hiddenfieldid != ''){
                   $("."+hiddenfieldid).val(id);
                   window.location.replace("/"+id);
               }

           }

       },
       open: function(e, ui){
          $(this).closest('li.open > .dropdown-menu').css('display','block');
       }

    });
}


/* Website Front Autocomplete ---------------------------------- */

function autocompleteWebsite(tablename,fieldname,fieldid,hiddenfieldid,url){

    $("."+fieldname).autocomplete({
        source: url+"/autocomplete/"+tablename+"/"+fieldname+"/"+fieldid,// path to method
        select: function ( event, ui ) {

            if (fieldid !=null) {
              
                var id= ui.item.id;
                if(hiddenfieldid != ''){
                    $("."+hiddenfieldid).val(id);

                }

            }

        }
    })
}

function autocompleteRedirectWebsite(tablename,fieldname,fieldid,hiddenfieldid,url){

    $("."+fieldname).autocomplete({
        source: url+"/autocomplete/"+tablename+"/"+fieldname+"/"+fieldid,// path to method
        select: function ( event, ui ) {
            if (fieldid !=null) {

                var id= ui.item.id;
                if(hiddenfieldid != ''){
                    $("."+hiddenfieldid).val(id);
                    window.location.replace("/"+id);
                }

            }

        },
        open: function(e, ui){
            $(this).closest('li.open > .dropdown-menu').css('display','block');
        }

    });
}
