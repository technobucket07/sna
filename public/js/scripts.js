var URL = window.location.origin;

function filterSchedules(venueName,courseSlug,csrfToken) {
    
     $('.loading-image-eq').show();
     window.location.href = URL+'/'+courseSlug+'/'+venueName+'#schedules';
}

// Location page scripts ---------------------------------------------

function filterByAlpha(alphabet) {

    var alphabets = $('.alphabets > li');
    var contentRows = $('.contentRows');
    var contentRow = $('.contentRows .contentRow a');
    var contentAdd = $('.contentAdd');
    
    alphabets.removeClass("active");
    $("#"+alphabet).addClass("active");
    
    contentRows.hide();
    contentAdd.html('');
    $(contentRow).each(function(){
        var current = $(this).text();
        if (RegExp('^'+alphabet).test(current)) {
            var link = $(this).attr('href');
            var content = '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 colm contentRow"><a href="'+link+'" title="">'+current+'</a></div>';
            contentAdd.html(contentAdd.html()+content);
        }
    });

    if(contentAdd.html() == '') { contentAdd.html('Sorry, None of the location starts with '+alphabet+'.'); }
}

function initializeMap(locations,mapmarker) {

    var country ="United Kingdom";
    var myOptions = {
      zoom: 5,
      center: new google.maps.LatLng(locations[0][1], locations[0][2]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    var map = new google.maps.Map($('#map')[0],myOptions);
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': country }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var infowindow = new google.maps.InfoWindow(); 
        var marker, i; 
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
                icon: mapmarker, 
                animation: google.maps.Animation.DROP, map: map });
            
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) { return function () { 
                    infowindow.setContent(locations[i][0]); infowindow.open(map, marker);
                } })(marker, i));
                
                google.maps.event.addListener(marker, 'mouseout', (function (marker, i) { return function () { 
                    infowindow.setContent(''); infowindow.close(map, marker);
                } })(marker, i));
            
            }   
      } else {
        alert("Could not find location: " + location);
      }
    });
}



// Modal Box Scripts ---------------------------------------------
function resetForm(formType,courseName='',additionalData='') {
    $('#modalForm').css('display','block');
    $('#alert').css('display','none');
    $('#success').css('display','none');
    $('#modalForm').trigger("reset");
    $('#formType').val(formType);
    $('#courseName').val(courseName);

    if(formType == 'Request CallBack') {
        $('#myModalLabel').text('REQUEST CALLBACK');
        $('#modalButton').text('Submit Request');
    }else if(formType == 'Get a Quote') {
        $('#myModalLabel').text('GET A QUOTE');
        $('#modalButton').text('Submit Request');
    }else {
        $('#myModalLabel').text('HAVE A QUERY ?');
        $('#modalButton').text('Submit Your Query');
        if(formType == 'Schedule') {       
            $('#schedule').val(additionalData);
        } else if(formType == 'Online Course' || formType == 'Online Course Booking') {    
            $('#package').val(additionalData);
        }
    } 
}
$('#modalForm').on('submit',function(e){ 
    var formValues = $(this).serialize();

    $('.loading-image-eq').show();

     $.ajax({
        url: URL+"/enquiry",
        type: "post",
        dataType : "JSON",
        data: formValues,
        success: function(data)
        {      
           $.each(data, function(key, value) {
                if(key == 'Errors') {
                    var errors = '';
                    $.each(value, function(i) {
                        errors += '<li>'+value[i]+'</li>';
                    })
                    $('#alert').html('<ul>'+errors+'</ul>');
                    $('#alert').css('display','block');
                }

                if(key == 'MailError') {
                    $('#alert').html('<ul><li>'+value+'</li></ul>');
                    $('#alert').css('display','block');
                }

                if(key == 'Success') {
                    $("#modalForm").css('display','none');
                    $('#success').css('display','block');
                }
            }) 
        },
        complete: function(){
            $('.loading-image-eq').hide();
        },
        error: function() {
            $('#alert').html('Sorry, Please try again later !');
            $('#alert').css('display','block');
        }
    });
     return false;
});




$(document).ready(function() {


// Course page scripts ---------------------------------------------


    /* Smooth Scroll ------------------------------------------------------ */
    $("#goto, #goto2, #goto3, #goto4, #goto5, #goto6, #goto1, #borws, #goToTop").on('click', function(event) 
    {
        event.preventDefault();
        var hash = this.hash;    
        $('html, body').animate({ scrollTop: $(hash).offset().top }, 800, 
            function() { window.location.hash = hash; });

    });

    /* Sticky ------------------------------------------------------ */
    $(window).scroll(function(){
        var sticky = $('.course-date'),
        scroll = $(window).scrollTop();

        if (scroll >= 430) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
        
         if(scroll <= 400)$("a#goToTop").addClass("godown");
        else $("a#goToTop").removeClass("godown");

    });

    /* Show More - Show Less ------------------------------------------------ */
    $(".text-toggle").each(function() {
        var textDivHeight = $(this).height();
        if(textDivHeight > 200){
            $(this).next(".action-toggle").css("display","block");
        } else{
            $(this).next(".action-toggle").css("display","none");
        }
    });
    $('.action-toggle').click(function(event) {
        event.preventDefault();
        var id = $(this).attr('href');
        $(id).toggleClass('toggle');
        
        if($(id).hasClass("toggle")) {   
            $(this).text("Show Less");
        } else {
            $(this).text("Show More");
        } 
    });




// Contact Form Scripts ---------------------------------------------

$('.enquiryForm').on('submit',function(e){ 
    var formValues = $(this).serialize();

     $('.loading-image-eq').show();
     $.ajax({
        url: URL+"/enquiry",
        type: "post",
        dataType : "JSON",
        data: formValues,
        success: function(data)
        {  
           $.each(data, function(key, value) {
                if(key == 'Errors') {
                    var errors = '';
                    $.each(value, function(i) {
                        errors += '<li>'+value[i]+'</li>';
                    })
                    $('.alert-danger').html('<ul>'+errors+'</ul>');
                    $('.alert-danger').show();
                }

                if(key == 'MailError') {
                    $('.alert-danger').html('<ul><li>'+value+'</li></ul>');
                    $('.alert-danger').show();
                }

                if(key == 'Success') {
                    $('.enquiryForm').hide();
                    $('.alert-danger').hide();
                    $('.alert-success').show();
                }
            }) 
        },
        complete: function(){
            $('.loading-image-eq').hide();
        },
        error: function() {
            $('.alert-danger').html('Sorry, Please try again later !');
            $('.alert-danger').show();
        }
    });
     return false;
});

// Search Scripts ---------------------------------------------

$(".hslug").val(''); // clear if has any value

$(".select2courses").select2({
      placeholder: "Search course here",
      tags: true,
      ajax: {
        url: URL+"/autocomplete/courses",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
          };
        },
        processResults: function (data) {
          return {
                results: $.map(data, function (item) {
                    return {
                        text: item.value,
                        id: item.id
                    }
                })
            }
        },
        cache: false
      },
      

    });

$(".select2courses").on("select2:open", function (e) { 
   $(this).closest('li.open > .dropdown-menu').css('display','block');
});

$(".select2courses").on("select2:close", function (e) { 
    $('.courseDisplayName').find('option').each(function(i,e){
        var textattr = $(this).attr('data-select2-tag');
        var hslug = $(".hslug").val();
        if(textattr && hslug == '') {
            var text = $(this).text();
            $('.select2courses').val(text).select2();
        }
    });
});

$(".select2courses.crs").on("select2:select", function (e) { 
    var hslug = this.value;
    var course = $(".courseDisplayName option:selected").text();
    if(hslug != course) {
        $(".hslug").val(hslug); 
        window.location.href=URL+'/'+hslug;
    }   
});
$('.select2courses.crs').on("change", function(e) {
    var hslug = this.value;
    var course = $(".courseDisplayName option:selected").text();
    if(hslug != course) {
        $(".hslug").val(hslug); 
        window.location.href=URL+'/'+hslug;
    } 
    $(this).closest('form').submit();
});


$(".select2courses.loc").on("select2:select", function (e) { 
    var hslug = this.value;
    $(".hslug").val(hslug); 
});

$('#locSearch').submit(function(e){

    var hslug = $(".hslug").val();
    var course = $(".courseDisplayName option:selected").text();
    var venue =  $("select[name='venue']").val().toLowerCase();
    
    // if only venue entered
    if(course == '' & venue != ''){
        e.preventDefault();
        $('.hslug').val();
        window.location.href=URL+'/training-locations/'+venue;
    }

    if(hslug == '' & course !='') {
        return;
    }else if(hslug != course) {       
        e.preventDefault();
        urlParam = hslug;
        if(venue != ''){
            urlParam += '/'+venue;
        }
        window.location.href=URL+'/'+urlParam;
    }    

});
 

// Navigation Slider ---------------------------------------------


    /* Level : 1 ----------------------------------------- */
    $('.slider-tab-menu').find('a').hover(function (e) {

       e.preventDefault();
       var index = $(this).index();
       $(this).siblings('a.active').removeClass("active");
       $(this).addClass("active");  
       $(this).closest('.slider-tab-menu').siblings('.slider-tab').find('.slider-tab-content').removeClass("active");
       $(this).closest('.slider-tab-menu').siblings('.slider-tab').find('.slider-tab-content').eq(index).addClass("active");
       $(this).closest('.slider-tab-menu').siblings('.slider-tab').find('.topcourses').removeClass("active");
       $(this).closest('.slider-tab-menu').siblings('.slider-tab').find('.slider-tab-content.active').find('.topcourses').first().addClass("active");
    });

    /* Level : 2 ----------------------------------------- */
    $('.slider-tab').find('.topcategories').hover(function (e) {

        e.preventDefault();   
        var index = $(this).index();  
        $(this).siblings('.topcategories.active').removeClass('active');
        $(this).addClass('active');
        $(this).closest('.lft-ul').siblings('.ryt-ul').find('.topcourses').removeClass("active");
        $(this).closest('.lft-ul').siblings('.ryt-ul').find('.topcourses').eq(index).addClass("active");
    });


    /* Navigation Active Only One ------------------------------------------ */
    $('#nav-courses').hover(function() {
        $('.s2  *').removeClass('active');
        $('.courses-active').addClass('active');
        $('.courses-active .ryt-ul .topcourses').first().addClass('active');
    });
    $('#nav-certificate').hover(function() {
        $('.s3  *').removeClass('active');
        $('.certificate-active').addClass('active');
        $('.certificate-active .ryt-ul .topcourses').first().addClass('active');
    });
    $('#nav-online').hover(function() {
        $('.s4  *').removeClass('active');
        $('.online-active').addClass('active');
        $('.online-active .ryt-ul .topcourses').first().addClass('active');
    });


    /* Navigation Home Visibilty --------------------------------------- */
    $(".jq").hover(function() {
        if($('#h-menu').css("visibility")=='hidden'){
            $('#h-menu').css("visibility", "visible");
        }
        else{
            $('#h-menu').css("visibility","hidden");
        }
    });


    /* Navigation Slider Mobile --------------------------------------- */
    if($(window).width() >= 1024){
        $(".s1 div.slider-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $(".s1 div.slider-tab>div.slider-tab-content").removeClass("active");
            $(".s1 div.slider-tab>div.slider-tab-content").eq(index).addClass("active");
        });

        $(".s2 div.slider-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $(".s2 div.slider-tab>div.slider-tab-content").removeClass("active");
            $(".s2 div.slider-tab>div.slider-tab-content").eq(index).addClass("active");
        });

        $(".s3 div.slider-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $(".s3 div.slider-tab>div.slider-tab-content").removeClass("active");
            $(".s3 div.slider-tab>div.slider-tab-content").eq(index).addClass("active");
        });

        $(".s4 div.slider-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $(".s4 div.slider-tab>div.slider-tab-content").removeClass("active");
            $(".s4 div.slider-tab>div.slider-tab-content").eq(index).addClass("active");
        });

    }



// Carousel ----------------------------------------------- 

$('.owl-carousel').owlCarousel({
loop: true,
margin: 2,
responsiveClass: true,
responsive: {
  0: {
    items: 1,
    nav: true
  },
  600: {
    items: 3,
    nav: false
  },
  1000: {
    items: 2,
    nav: true,
    loop: false,
    margin: 2
  }
}
});

           
}); // document ready

