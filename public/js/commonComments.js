var URL = window.location.origin + '/';

function getAllComments(entity,entity_id,sub_entity){
    $.ajax({
        url: URL + "get/comments?entity="+entity+"&entityId="+entity_id+"&sub_entity="+sub_entity,
        type: 'get',
        success: function (response) {
            $('#'+sub_entity+'CommentList').html(response);
        }
    });
}

function storeComment(entity,entity_id,sub_entity){
    var message = $('#'+sub_entity+'Comment').val();
    $('#'+sub_entity+'Comment').removeClass('red-border');
    if(message == ''){
        $('#'+sub_entity+'Comment').addClass('red-border');
        return false;
    }
    if(sub_entity != 'undefined' || sub_entity != ''){
        var finalUrl = URL+'add/comment?entity='+entity+'&entity_id='+entity_id+'&sub_entity='+sub_entity+'&message='+message;
    }else{
        var finalUrl = URL+'add/comment?entity='+entity+'&entity_id='+entity_id+'&message='+message;
    }
    $.ajax({
        url:finalUrl,
        type: 'GET',
        success: function (response) {
            getAllComments(entity,entity_id,sub_entity);
            $('#'+sub_entity+'Comment').val('');
        },
        error: function(response) {
            var errors = response;
            var errors = (response.responseJSON);
            var error ='';
            jQuery.each(errors, function (i, val) {
                error = error + '<li>'+val+'</li>';
            })
            error =  error+"</ul>";
        }
    })
}