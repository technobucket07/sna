var URL = window.location.origin+'/';

function openModalBox(enquiry){
    $('#modalId').val(enquiry);
    $('#myModal').modal('show');
    $('.select2').select2();
}

function openCommentModalBox(enquiry){
    $('#entity_id').val(enquiry);
    $('#commentModalId').modal('show');
    getComments(enquiry);
}

function getComments(enquiry){
    $.ajax({
        url: URL + "get/comments?entity=lead&entityId="+enquiry,
        type: 'get',
        success: function (response) {
            $('#comments').html(response);
        }
    });
}

function saveComment(entity){
    var entity_id = $('#entity_id').val();
    var message = $('#newComment').val();
    if(message == ''){
        $('#newComment').addClass('red-border');
        return false;
    }
    $.ajax({
        url:URL+'add/comment?entity='+entity+'&entity_id='+entity_id+'&message='+message,
        type: 'GET',
        success: function (response) {
            getComments(entity_id);
            $('#newComment').val('');
        },
        error: function(response) {
            var errors = response;
            var errors = (response.responseJSON);
            var error ='';
            jQuery.each(errors, function (i, val) {
                error = error + '<li>'+val+'</li>';
            })
            error =  error+"</ul>";
        }
    })
}