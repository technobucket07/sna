<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customerbillingdetails extends Model
{
	use SoftDeletes;
    protected $table='customerbillingdetails';
    protected $guarded= ['BillingDetailID'];
    protected $primaryKey='BillingDetailID';

    public function billingCountry($cId) {
    	$countryName = Countries::where('id', $cId)->value('name');
        return $countryName;
    }
}
