<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Setting extends Model
{
    use SoftDeletes;
    use LogsActivity;
    protected $table='companyInfo';
    protected $guarded= ['companyId'];
    protected $primaryKey='companyId';

    protected $fillable = ['companyName', 'email'];

    protected static $logAttributes = ['companyName', 'email'];
}
