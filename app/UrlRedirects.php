<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UrlRedirects extends Model
{
    //use SoftDeletes;
    protected $table='urlRedirects';
    protected $guarded= ['id'];
    protected $primaryKey='id';
}
