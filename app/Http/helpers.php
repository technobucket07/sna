<?php

use App\User;
use App\Course;
use App\Countries;
use App\Instructor;
use App\EventVenue;
use App\Location;
use App\Enquiries;
use App\Payment;


function getUserAttrUsingId($id){
    return User::where('id', $id)->pluck('name')->first();
}

function getCourseAttrUsingId($id, $attr){
    return Course::where('courseId', $id)->pluck($attr)->first();
}

function getEnquiryAttrUsingId($id, $attr){
    return Enquiries::where('enquiryId', $id)->pluck($attr)->first();
}

function getPaymentDetailsAttrUsingLeadId($id, $attr){
    return Payment::where('fk_leadId', $id)->pluck($attr)->first();
}

function getCountryAttrUsingId($id, $attr){
    return Countries::where('countryId', $id)->pluck($attr)->first();
}

function getInstructorAttrUsingId($id, $attr){
    return Instructor::where('id', $id)->pluck($attr)->first();
}

function getVenueAttrUsingId($id, $attr){
    return EventVenue::where('id', $id)->pluck($attr)->first();
}

function getLocationAttrUsingId($id, $attr){
    return Location::where('id', $id)->pluck($attr)->first();
}

function getOutput($input){
    if($input > 0){
        return 'Yes';
    }else{
        return 'No';
    }
}


function generateEventNumber($id){
    return 'SNA0'.$id;
}
