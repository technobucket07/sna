<?php

namespace App\Http\Controllers;

use App\Course;
use App\Location;
use App\Schedules;
use App\Venue;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ip= \Request::ip();
        $data = \Location::get($ip);
        return redirect('/admin/dashboard');
    }


    public function showCalender()
    {
        $data = Schedules::where('responseDate','>=', Carbon::now()->format('Y-m-d'))->get();
        $str = '';
        foreach ($data as $d)
        {
            $cn = Course::where('courseId',$d['fk_courseId'])->first();
            $vn = Location::where('id',$d['fk_locationId'])->first();
            if($d['fk_venueId'] == null && $d['fk_instructorIds'] == null)
            {
                $str .= "{ title : '".$cn['courseDisplayName']."', start :  new Date(".Carbon::parse($d['responseDate'])->format('Y').",".(Carbon::parse($d['responseDate'])->format('m') - 1) .",".Carbon::parse($d['responseDate'])->format('j').") , backgroundColor: '#dd4b39', borderColor    : '#dd4b39'   ,             
                dat : '".Carbon::parse($d['responseDate'])->format('j M Y')."' ,
                loc: '<li>".$vn['name']."</li>' } , ";
            }
            elseif (($d['fk_venueId'] != null && $d['fk_instructorIds'] == null) || ($d['fk_venueId'] == null && $d['fk_instructorIds'] != null))
            {
                $str .= "{ title : '".$cn['courseDisplayName']."', start :  new Date(".Carbon::parse($d['responseDate'])->format('Y').",".(Carbon::parse($d['responseDate'])->format('m') - 1) .",".Carbon::parse($d['responseDate'])->format('j').") , backgroundColor: '#f39c12', borderColor    : '#f39c12'   ,             
                dat : '".Carbon::parse($d['responseDate'])->format('j M Y')."' ,
                loc: '<li>".$vn['name']."</li>' } , ";
            }
            else
            {
                $str .= "{ title : '".$cn['courseDisplayName']."', start :  new Date(".Carbon::parse($d['responseDate'])->format('Y').",".(Carbon::parse($d['responseDate'])->format('m') - 1) .",".Carbon::parse($d['responseDate'])->format('j').") , backgroundColor: '#00a65a', borderColor    : '#00a65a'   ,             
                dat : '".Carbon::parse($d['responseDate'])->format('j M Y')."' ,
                loc: '<li>".$vn['name']."</li>' } , ";
            }

        }
        return view('admin.calender',compact('str'));
    }
}
