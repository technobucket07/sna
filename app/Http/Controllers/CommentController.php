<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class CommentController extends Controller
{
    public function getComments()
    {
        $entity = Input::get('entity');
        $entityId = Input::get('entityId');
        $sub_entity = Input::get('sub_entity');
        if($sub_entity != ''){
            $comments = Comment::where(['entity'=>$entity, 'entity_id'=>$entityId, 'sub_entity'=>$sub_entity])->get();
        }else{
            $comments = Comment::where(['entity'=>$entity, 'entity_id'=>$entityId])->get();
        }

        $data = '';
        foreach($comments as $comment){
            $class='left';
            $time = Carbon::parse($comment['created_at'])->format('d-M-Y H:i A');
            if($comment['user_id'] != \Auth::user()->id){
                $data .= '<div class="direct-chat-msg left">'.
                    '<div class="direct-chat-info clearfix">'.
                    '<span class="direct-chat-name pull-left">'.getUserAttrUsingId($comment['user_id']).'</span>'.
                    '</div>'.
                    '<div class="direct-chat-text clearfix" style="padding-bottom: 20px; margin-left: 17px;">'.
                    '<div>'.$comment['comment'].'</div>'.
                    '<span class="pull-left direct-chat-timestamp">'.$time.'</span>'.
                    '</div>'.
                    '</div>';
            }else{
                $data .= '<div class="direct-chat-msg right">
                             <div class="direct-chat-info clearfix">
                                 <span class="direct-chat-name pull-right">'.getUserAttrUsingId($comment['user_id']).'</span>
                             </div>
                             <div class="direct-chat-text clearfix" style="padding-bottom: 20px; margin-right: 17px;">
                                  <div>'.$comment['comment'].' </div>
                                  <span class="pull-right ">'.$time.'</span>
                              </div>
                         </div>';
            }

        }
        return $data;
    }

    public function addComment()
    {
        $entity = Input::get('entity');
        $entityId = Input::get('entity_id');
        $comment = Input::get('message');
        $sub_entity = Input::get('sub_entity');
        Comment::create(['entity'=>$entity, 'entity_id'=>$entityId,'sub_entity'=>$sub_entity, 'comment'=>$comment, 'user_id'=>\Auth::user()->id]);
        return ;

    }
}
