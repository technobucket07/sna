<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Instructor;

class InstructorController extends Controller
{
    public function details($id='')
    {
        $instructorDetails = [];
        $picturePath = 'images/instructors/default.png';
        $cvPath = '';
        if($id != ''){
            $instructorDetails = Instructor::find($id);
            if($instructorDetails['image'] != ''){
                $picturePath = 'images/instructors/pictures/'.$instructorDetails['image'];
            }
            if($instructorDetails['CV'] != ''){
                $cvPath = 'images/instructors/CV/'.$instructorDetails['CV'];
            }
        }
        return view('admin.instructor.instructor', compact('instructorDetails', 'picturePath', 'cvPath'));
    }

    public function submitInstructorDetails(Request $request)
    {
        $this->validateInstructorDetails($request);
        $input = $request->input();
        unset($input['_token']);
        if($request->hasFile('image')) {
            $file  = $request->file('image');
            $input['image'] = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/images/instructors/pictures');
            $file->move($destinationPath, $input['image']);
        }

        if($request->hasFile('CV')) {
            $file  = $request->file('CV');
            $input['CV'] = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/images/instructors/CV');
            $file->move($destinationPath, $input['CV']);
        }
        Instructor::updateOrCreate(['id'=>$input['id']], $input);
        if($input['id'] != ''){
            $log = 'Instructor: '.$input['first_name'].' '.$input['last_name']. ' Updated ';
        }else{
            $log = 'Instructor: '.$input['first_name'].' '.$input['last_name']. ' Added ';
        }
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function validateInstructorDetails($request)
    {
        $id = $request->input('id');
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:instructors,email,'.$id.',id',
            'phone' =>  'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'linkedIn_profile' => 'required',
            'skills' => 'required',
            'certifications' => 'required',
            'gender' => 'required|string|in:Male,Female',
        ];
        if($id != null){
           $user = Instructor::find($id);
            if ($user['image'] == ''){
                $rules['image'] = 'required|image';
            }else if($request->hasFile('image')){
                $rules['image'] = 'image';
            }
        }else{
            $rules['image'] ='required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }
        $this->validate($request, $rules);
    }

    public function instructorList()
    {
        $instructorList = Instructor::paginate(20);
        return view('admin.instructor.list', compact('instructorList'));
    }

    public function deleteInstructor($id)
    {
        $instructor  = Instructor::find($id);
        unlink(public_path('/images/instructors/pictures/'.$instructor['image']));
        unlink(public_path('/images/instructors/CV/'.$instructor['CV']));
        $instructor->delete();
        activity()->log('Instructor Deleted');
        \Session::flash('flash_message','Instructor Deleted.');
        return redirect()->back();
    }
}
