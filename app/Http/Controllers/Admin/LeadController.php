<?php

namespace App\Http\Controllers\Admin;

use App\Countries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enquiries;
use App\User;
use App\Course;
use App\Payment;

class LeadController extends Controller
{

    public function details($id=null)
    {
        $leadDetails = [];
        $users = [];
        if($id != ''){
            $leadDetails = Enquiries::find($id);
        }
        $course = Course::pluck('courseName', 'courseId')->toArray();
        return view('admin.lead.details', compact('leadDetails', 'users', 'course'));
    }

    public function leadList()
    {
        $assignedTo = '';
        $users = [''=>''] + User::pluck('name', 'id')->toArray();
        $enquiries = Enquiries::whereNull('assignedTo')->paginate(20);
        return view('admin.lead.list', compact('enquiries', 'users', 'assignedTo'));
    }

    public function assignedList()
    {
        $assignedTo = 'alreadyAssigned';
        $users = [''=>''] + User::pluck('name', 'id')->toArray();
        $enquiries = Enquiries::whereNotNull('assignedTo')->paginate(20);
        return view('admin.lead.list', compact('enquiries', 'users', 'assignedTo'));
    }

    public function myLeadList()
    {
        $enquiries = Enquiries::where('assignedTo', \Auth::user()->id)->paginate(20);
        return view('admin.lead.myleads', compact('enquiries'));
    }


    public function assignLead(Request $request)
    {
        $input = $request->input();
        if(isset($input['assignTo'])){
            $assignTo = $input['assignTo'];
        }else{
            $assignTo = \Auth::user()->id;
        }
        foreach($input['enquiryId'] as $enquiryId){
            Enquiries::where('enquiryId', $enquiryId)->update(array('assignedTo'=>$assignTo));
        }

        $log = 'Lead Assigned. ';
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function followedUp(Request $request)
    {
        $input = $request->input();
        $followedUpBy = \Auth::user()->id;
        Enquiries::where('enquiryId', $input['enquiryId'])->update(array('followedUpBy'=>$followedUpBy, 'isFollowedUp'=>1));
        $log = 'Lead Followed Up. ';
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function markInterest(Request $request)
    {
        $input = $request->input();
        $interestMarkedBy = \Auth::user()->id;
        $isInterested = 0;
        if(isset($input['Yes'])){
            $isInterested = 1;
        }

        Enquiries::where('enquiryId', $input['enquiryId'])->update(array('interestMarkedBy'=>$interestMarkedBy, 'isInterested'=>$isInterested));
        $log = 'Interest Marked. ';
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function markConversionStatus(Request $request)
    {
        $input = $request->input();
        $convertedBy = \Auth::user()->id;
        $isConverted = 0;
        if(isset($input['Yes'])){
            $isConverted = 1;
        }

        Enquiries::where('enquiryId', $input['enquiryId'])->update(array('convertedBy'=>$convertedBy, 'isConverted'=>$isConverted));
        $log = 'Conversion Status Updated. ';
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function submitLeadDetails(Request $request)
    {
        $this->validateLeadDetails($request);
        $input = $request->input();
        unset($input['_token']);

        if($input['enquiryId'] != ''){
            $log = 'Lead: '.$input['name'].' Updated ';
        }else{
            $log = 'Lead: '.$input['name'].' Added ';
            $input['assignedTo'] = \Auth::user()->id;
            $input['type'] = 'myLead';
        }
        Enquiries::updateOrCreate(['enquiryId'=>$input['enquiryId']], $input);
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function validateLeadDetails($request)
    {
        $id = $request->input('enquiryId');
        $rules = [
            'name' => 'required',
            'email' => 'required',
            //'email' => 'required|unique:instructors,email,'.$id.',id',
            'phone' =>  'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'company' => 'required',
            'course' => 'required',
            'delegates' => 'required',
            'address' => 'required',
            'location' => 'required',
        ];
        $this->validate($request, $rules);
    }

    public function deleteLead($id)
    {
        $lead  = Enquiries::find($id);
        $lead->delete();
        activity()->log('Lead Deleted');
        \Session::flash('flash_message','Lead Deleted.');
        return redirect()->back();
    }

    public function convertedLeads()
    {
        $convertedLeads = Enquiries::where('isConverted', 1)->paginate(50);
        return view('admin.lead.convertedLeads', compact('convertedLeads'));
    }

    public function paymentDetails($leadId)
    {
        $paymentTypes = array(''=>'','Full'=>'Full','Partial'=>'Partial');
        $currencies = [''=>''] + Countries::where('isOperative', 1)->pluck('currencyCode', 'currencyCode')->toArray();
        $paymentDetails = Payment::where('fk_leadId', $leadId)->first();
        $leadDetails = Enquiries::find($leadId);
        return view('admin.lead.paymentDetails', compact('paymentDetails', 'leadId', 'paymentTypes', 'currencies', 'leadDetails'));
    }

    public function paymentDetailsSubmit(Request $request)
    {
        $id = $request->input('id');
        $rules = [
            'payment_type' => 'required',
            'currency_code' => 'required',
            'total_amount' =>  'required|numeric',
            'paid_amount' =>  'required|numeric',
        ];
        $this->validate($request, $rules);

        $input = $request->input();
        unset($input['_token']);
       Payment::updateOrCreate(['id'=>$id], $input);
        if($id == ''){
            $log = "Payment added";
        }else{
            $log = "Payment updated";
        }
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

   /* public function paymentList()
    {
        $paymentList = Payment::paginate(50);
        return view('admin.lead.paymentList', compact('paymentList'));
    }*/
}
