<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{
    public function dashboard()
    {
        $id  = Setting::All()->max('companyId');
        $result=  Setting::where('companyId',$id)->get();
        return view ('admin.dashboard',compact('result'));
    }
}
