<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Input;
use App\User;

class AdminUserController extends Controller
{
    public function index()
    {
        return view ('admin.changePassword');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);
        $input= $request->all();
        //dd($input);

        $input['password']= Hash::make($input['password']);

        User::where('email', Auth::user()->email)->update(['password' => $input['password']]);

        \Session::flash('flash_message','Password Updated.');

        activity()->log('Password Updated for : '. Auth::user()->email);

        return redirect()->route('AdminChangePassRoute');
    }

    public function userList()
    {
        $id = Input::get('id');
        $data['userDetais'] = [];
        $data['rolesAssigned'] = [];
        if($id != ''){
           $data['userDetais'] =  User::find($id);
           $data['rolesAssigned'] = $data['userDetais']->getRoleNames();
        }
        $data['roles'] = Role::pluck('name', 'name')->toArray();
        $data['userList'] = User::all();
        return view ('admin.user.list', $data);
       // $user->assignRole('writer');
    }

    public function submitUserDetails(Request $request)
    {
        $id = $request->input('id');
        $this->validate($request, [
            "name" => "required",
            "email" => 'required|unique:users,email,'.$id.',id',
            "roles" => "required",
        ]);
        $input = $request->input();
        $roles = $input['roles'];
        unset($input['_token']);
        unset($input['roles']);
        if($id == ''){
            $input['password'] =  Hash::make('Welcome@123');
        }
        $user = User::updateOrCreate(['id'=>$input['id']], $input);
        $user->syncRoles($roles);
        if($input['id'] != ''){
            $log = 'User: '.$input['name'].' Updated ';
        }else{
            $log = 'User: '.$input['name'].' Added ';
        }
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function deleteUser()
    {
        $id = Input::get('id');
        $user = User::find($id);
        $roles = $user->getRoleNames();
        foreach($roles as $role){
            $user->removeRole($role);
        }

        $user->delete();
        $log = 'User: '.$user['name'].' Deleted ';
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }
}
