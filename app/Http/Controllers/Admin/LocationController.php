<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Countries;
use App\Location;

class LocationController extends Controller
{
    public function locationList()
    {
        $locationList = Location::paginate(30);
        return view('admin.location.list', compact('locationList'));
    }

    public function details($id='')
    {
        $locationDetails = [];
        if($id != ''){
            $locationDetails = Location::find($id);
        }
        $countries = [''=>'']+Countries::where('isOperative', 1)->pluck('name', 'countryId')->toArray();
        return view('admin.location.details', compact('locationDetails', 'countries'));
    }

    public function submitLocationDetails(Request $request)
    {
        $id = $request->input('id');
        $this->validate($request, [
            "fk_countryId" => "required",
            "name" => 'required|unique:locations,name,'.$id.',id',
        ],[
            'fk_countryId.required' => 'Country is required',
            'name.required' => 'Location name is required',
        ]);
        $input = $request->input();
        unset($input['_token']);
        Location::updateOrCreate(['id'=>$input['id']], $input);

        if($input['id'] != ''){
            $log = 'Location: '.$input['name'].' Updated ';
        }else{
            $log = 'Location: '.$input['name'].' Added ';
        }
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }
}
