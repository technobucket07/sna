<?php

namespace App\Http\Controllers\Admin;

use App\EventVenue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Schedules;
use App\Instructor;
use DB;
use App\Course;

class EventController extends Controller
{
    public function eventList()
    {
        $events = Schedules::whereNotNull('scheduleId')->orderBy('responseDate','desc')->paginate();
        return view('admin.event.list', compact('events'));
    }

    public function eventDetails($id)
    {
        $eventDetails = Schedules::find($id);
        $assignableVenues = [];
        $assignedInstructors = [];
        if($eventDetails['fk_venueId'] == null){
            $venues = EventVenue::where('fk_countryId', $eventDetails['fk_countryId'])->get();
            $assignableVenues['']='';
            foreach($venues as $venue){
                $assignableVenues[$venue['id']] =  $venue['venueName']. " - ".getLocationAttrUsingId($venue['location'], 'name');
            }

        }

        if($eventDetails['fk_instructorIds'] != null){
            $instructorIds = explode(", ",$eventDetails['fk_instructorIds']);
            foreach($instructorIds as $instructorId){
                if($instructorId != ''){
                    $assignedInstructors[$instructorId] = $instructorId;
                }
            }
        }
        $assignableInstructors = Instructor::select([ DB::raw("CONCAT(first_name,' ',last_name)  AS name"),'id'])->get()->pluck('name', 'id')->toArray();
        return view('admin.event.eventDetails', compact('eventDetails', 'assignableInstructors', 'assignableVenues', 'assignedInstructors'));
    }

    public function assignVenue(Request $request)
    {
        $this->validate($request, [
            "fk_venueId" => "required",
        ], ['fk_venueId.required'=>'Please Select Venue']);
        $input = $request->input();
        $scheduleDetails = Schedules::find($input['scheduleId']);
        $scheduleDetails->update(array('fk_venueId'=>$input['fk_venueId'], 'venueAssignedBy'=>\Auth::user()->id));
        activity()->log('Venue Assigned for Course : '.Course::get_Course_Display_Name($scheduleDetails['fk_courseId']).' at Location '.getLocationAttrUsingId($scheduleDetails['fk_locationId'], 'name'));
        \Session::flash('flash_message','Venue Assigned Successfully.');
        return redirect()->back();
    }

    public function assignInstructor(Request $request)
    {
        $input = $request->input();
        $this->validate($request, [
            "fk_instructorIds" => "required",
        ], ['fk_instructorIds.required'=>'Please Select Instructor']);
        $scheduleDetails = Schedules::find($input['scheduleId']);
        $instructorIds = null;
        if(isset($input['fk_instructorIds'])){
            foreach($input['fk_instructorIds'] as $instId){
                $instructorIds.=$instId.',';
            }
        }

        $instructorIds = rtrim($instructorIds,',');
        if($instructorIds != ''){
            $scheduleDetails->update(array('fk_instructorIds'=>$instructorIds, 'instructorAssignedBy'=>\Auth::user()->id));
        }else{
            $scheduleDetails->update(array('fk_instructorIds'=>$instructorIds, 'instructorAssignedBy'=>null));
        }
        activity()->log('Instructor Assigned for Course : '.Course::get_Course_Display_Name($scheduleDetails['fk_courseId']).' at Location '.getLocationAttrUsingId($scheduleDetails['fk_locationId'], 'name'));
        \Session::flash('flash_message','Venue Assigned Successfully.');
        return redirect()->back();
    }
}


