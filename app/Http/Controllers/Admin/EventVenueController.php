<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Countries;
use App\Features;
use App\VenueFeature;
use App\EventVenue;
use Zend\Validator\File\Count;
use App\Location;

class EventVenueController extends Controller
{
    public function venueList()
    {
        $venueList = EventVenue::where('id','<>',0)->orderby('displayOrder','asc')->get();
        return view ('admin.event_venue.listVenues',compact('venueList'));
    }

    public function details($id='')
    {
        $venueDetails = [];
        $locations = [];
        $selectedFeatures = [];
        $picturePath ='';
        if($id != ''){
            $venueDetails = EventVenue::find($id);
            $locations = Location::where(['fk_countryId'=>$venueDetails['fk_countryId']])->pluck('name', 'id')->toArray();
            $selectedFeatures =  VenueFeature::where('fk_venueId', $id)->pluck('fk_featureId','fk_featureId')->toArray();
            $picturePath = 'images/venue/'.$venueDetails['image'];
        }
        $countries = [''=>'']+Countries::where('isOperative', 1)->pluck('name', 'countryId')->toArray();
        $features = Features::pluck('name', 'id')->toArray();
        return view('admin.event_venue.addVenue', compact('venueDetails', 'countries', 'features', 'locations', 'selectedFeatures', 'picturePath'));
    }


    public function submitVenueDetails(Request $request)
    {
        $this->validate($request, [
            "venueName" => "required",
            "address" => "required",
            "phone" => "required",
            "email" => "required",
            "venueDescription" => "required" ,
        ]);
        $input = $request->input();
        unset($input['_token']);
        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            $input['image'] = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/images/venue');
            $file->move($destinationPath, $input['image']);
        }
        $features = $input['fk_featureId'];
        unset($input['fk_featureId']);

        $venueDetails = EventVenue::updateOrCreate(['id'=>$input['id']], $input);

        VenueFeature::whereNotIn('fk_featureId', $features)->where('fk_venueId', $venueDetails['id'])->delete();
        foreach($features as $feature){
            VenueFeature::firstOrCreate(array('fk_venueId'=> $venueDetails['id'], 'fk_featureId'=>$feature));
        }
        if($input['id'] != ''){
            $log = 'EventVenue: '.$input['venueName'].' Updated ';
        }else{
            $log = 'EventVenue: '.$input['venueName'].' Added ';
        }
        activity()->log($log);
        \Session::flash('flash_message',$log);
        return redirect()->back();
    }

    public function deleteVenue($venueId)
    {
        activity()->log('EventVenue: '.getVenueAttrUsingId($venueId, 'venueName').' is Deleted');
        EventVenue::where('id',$venueId)->delete();
        VenueFeature::where('fk_venueId', $venueId)->delete();
        \Session::flash('flash_message','Venue Deleted');
        return redirect()->back();
    }

}
