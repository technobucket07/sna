<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Schedules;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Countries;
use Illuminate\Support\Facades\Validator;
use App\Venue;
use App\Location;

class ScheduleController extends Controller
{
    public function index()
    {
        //dd('r');
        $result = Course::all();
       // dd($result);
        return view('admin.listSchedules',compact('result'));
    }

    public function addSchedule()
    {
         $courseList = [''=>''] + Course::pluck('courseDisplayName', 'courseId')->toArray();
         //$venueList = Venue::all();
         $countries = [''=>'']+ Countries::where('isOperative', 1)->pluck('name', 'countryId')->toArray();
        return view('admin.addSchedule',compact('courseList','countries'));
    }

    public function addScheduleData(Request $request)
    {
        $rules = [
            'locations' =>'required',
            'fk_courseId' =>'required',
            'price' =>'required',
            'duration' =>'required',
            'dates' =>'required'
        ];

        $messages= [
            'locations.required' => 'Please Select Location'
        ];

        $input = $request->all();

        Validator::make($input, $rules, $messages)->validate();


        $dates = explode(',',$input['dates']);

        foreach ($input['locations'] as $locationId)
        {
            foreach ($dates as $d)
            {
                Schedules::create([
                    'fk_courseId' => $input['fk_courseId'],
                    'eventPrice' => $input['price'],
                    'duration' => $input['duration'],
                    //'eventTime' =>  $input['time'],
                    'fk_locationId' => $locationId,
                    'fk_countryId' => $input['fk_countryId'],
                    'responseDate' => Carbon::parse($d)->format('Y-m-d')
                ]);
            }

        }

        return redirect('/admin/listschedules');

    }

    public function displaySchedules(Request $request)
    {

        $input = $request->all();
        unset($input['_token']);
       // dd($input);
        $result =  $result = Course::all();
        $listschedule = Schedules::where('fk_courseId',$input['fk_courseId'])->where('fk_locationId','<>',0)->get();
        $cid=$input['fk_courseId'];
        // dd($listschedule);
        return view('admin.listSchedules',compact('result','listschedule','cid'));
    }

    public function editSchedule($scheduleId)
    {

        //$venueList = Venue::all();
        $scheduleDetail = Schedules::where('scheduleId', $scheduleId)->first()->toarray();
        $countries = [''=>'']+ Countries::where('isOperative', 1)->pluck('name', 'countryId')->toArray();
        $locations = [''=>'']+ Location::where('fk_countryId', $scheduleDetail['fk_countryId'])->pluck('name', 'id')->toArray();
        return view('admin.editSchedules',compact('countries','scheduleDetail', 'locations'));
    }

    public function updateSchedule($scheduleId,Request $request){
        $this->validation($request);
        $input = $request->all();
        $cid = $input['courseId'];
        unset($input['_token']);
        unset($input['courseId']);
       // dd($input);
        Schedules::where('scheduleId',$scheduleId)->update($input);


        activity()->log('Schedule For: '.Course::get_Course_Display_Name($cid).' is Updated for '.getLocationAttrUsingId($input['fk_locationId'], 'name'));

        \Session::flash('flash_message','Schedule Successfully Updated.');

        return Redirect('/admin/listschedules');


    }

    public function deleteSchedule($scheduleId)
    {
        $res = Schedules::where('scheduleId',$scheduleId)->get();
        activity()->log('Schedule For: '.Course::get_Course_Display_Name($res[0]['fk_courseId']).' is Deleted for '.getLocationAttrUsingId($res[0]['fk_locationId'], 'name'));
        Schedules::where('scheduleId',$scheduleId)->delete();
         \Session::flash('flash_message','Schedule Successfully Deleted.');
        return Redirect('/admin/listschedules');
    }

    public function validation($request)
    {
        $rules = [
            'fk_locationId' =>'required',
            'responseDate' =>'required',
            'eventPrice' =>'required',
            'duration' =>'required'
        ];

        $messages= [
            'fk_locationId.required' => 'Please Select Location'
        ];
       return $this->validate($request, $rules, $messages);
    }


}
