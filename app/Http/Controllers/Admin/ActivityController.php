<?php

namespace App\Http\Controllers\Admin;

use App\ActivityLogs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    //
    public function index()
    {
        $result = ActivityLogs::orderBy('id','DESC')->get();
       // dd($result);
        return view ('admin.listActivityLog',compact('result'));
    }
}
