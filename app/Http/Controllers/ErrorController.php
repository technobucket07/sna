<?php

namespace App\Http\Controllers;

class ErrorController extends Controller
{
    public function __construct() {
    }
    
    public function index() {
    	$details = $this->pagedetails();
    	return abort(404);
        return view ('errors.404',$details);
    }
}
