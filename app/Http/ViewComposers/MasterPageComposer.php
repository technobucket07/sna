<?php

namespace App\Http\ViewComposers;
use App\Http\Controllers;
use App\Setting;
use Illuminate\View\View;




class MasterPageComposer
{
    /**
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $id  = Setting::All()->max('companyId');
        $pageDetails =  Setting::where('companyId',$id)->get();
        $view->with(compact('result','pageDetails'));
    }
}