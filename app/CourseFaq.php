<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseFaq extends Model
{
    use SoftDeletes;
    use \Rutorika\Sortable\SortableTrait;
    protected $table='courseFaq';
    protected $guarded= ['faqId'];
    protected $primaryKey='faqId';

    protected static $sortableField = 'faqSortOrder';

    public function course() {
    	return $this->belongsTo('App\Course');
    }

    public static function get_faq_name($id)
    {
        $name= CourseFaq::where('faqId',$id)->value('faqQuestion');
        return $name;
    }
}
