<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orderpaymentmethods extends Model
{
	use SoftDeletes;
    protected $table='orderpaymentmethods';
    protected $guarded= ['PaymentMethodID'];
    protected $primaryKey='PaymentMethodID';
}
