<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventVenue extends Model
{
    protected $table='event_venue';
    protected $guarded= ['id'];
    protected $primaryKey='id';
}
