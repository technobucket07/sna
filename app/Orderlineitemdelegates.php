<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orderlineitemdelegates extends Model
{
	use SoftDeletes;
    protected $table='orderlineitemdelegates';
    protected $guarded= ['DelegateID'];
    protected $primaryKey='DelegateID';
}
