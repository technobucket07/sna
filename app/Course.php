<?php

namespace App;

use App\Scopes\CourseOrderScope;
use App\Scopes\CoursePublishScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class Course extends Model
{
    use SoftDeletes;
    use \Rutorika\Sortable\SortableTrait;
    protected $table='course';
    protected $guarded= ['courseId'];
    protected $primaryKey='courseId';

    protected static $sortableField = 'position';

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new CourseOrderScope);
        $reqUri=request()->getRequestUri();
        if (!str_contains($reqUri,'admin')){
            static::addGlobalScope(new CoursePublishScope);
        }
    }

    public function courseBulletPoint() {
    	return $this->hasMany('App\CourseBulletPoint','fk_courseId');
    }
    public function courseReview() {
    	return $this->hasMany('App\CourseReview','fk_courseId');
    }
    public function courseFaq() {
    	return $this->hasMany('App\CourseFaq','fk_courseId');
    }
    public function schedules() {
        return $this->hasMany('App\Schedules','fk_courseId');
    }

    public function courseWhatsIncluded() {
        //return $this->hasMany('App\CourseWhatsIncludedRelation','fk_courseId');
        return $this->belongsToMany('App\CourseWhatsIncluded','courseWhatsIncludedRelation','fk_courseId','fk_whatsIncludedId');
    }
    
    public function courseResources() {
        return $this->belongsToMany('App\Resources','courseResourceRelation','fk_courseId','fk_resourceId');
    }

    public function courseChildren()
    {
        return $this->belongsToMany('App\Course','parentCourseRelation','fk_parentCourseId','fk_courseId');
    }



    public function courseParents()
    {
        return $this->belongsToMany('App\Course','parentCourseRelation','fk_courseId','fk_parentCourseId');
    }

    public function popularCourses()
    {
        return $this->hasMany('App\PopularCourses','fk_courseID','courseId');
    }

    public function coursesInDemand()
    {
        return $this->hasMany('App\CoursesInDemand','fk_courseID','courseId');
    }

    public function courseRelation() {
        return $this->hasMany('App\ParentCourseRelation','fk_courseId','courseId');
    }


    public function courseDeliveryMethod() {
        return $this->belongsToMany('App\CourseDeliveryMethod','courseDeliveryMethodRelation','fk_courseId','fk_methodId');
    }

    public static function get_Course_Name($cId){

        $courseName = Course::where('courseId', $cId)->value('courseName');
        return $courseName;

    }


    public static function get_Course_Display_Name($cId){

        $courseName = Course::where('courseId', $cId)->value('courseDisplayName');
        return $courseName;

    }

    public static function getPopularCourses()
    {
        return PopularCourses::all();
    }

    public static function getCoursesInDemand()
    {
        return CoursesInDemand::all();
    }

    public static function has_WhatsInc($cId,$wid)
    {
        $res = DB::table('courseWhatsIncludedRelation')->where('fk_courseId',$cId)->where('fk_whatsIncludedId',$wid)->count();
        if($res>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    public static function has_Resource($cId,$rid)
    {
        $res = DB::table('courseResourceRelation')->where('fk_courseId',$cId)->where('fk_resourceId',$rid)->count();
        if($res>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public static function is_Popular($cId)
    {
        $res = DB::table('popularcourses')->where('fk_courseID',$cId)->where('fk_countryID','IN')->count();
        if($res>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public static function is_InDemand($cId)
    {
        $res = DB::table('coursesInDemand')->where('fk_courseID',$cId)->where('fk_countryID','IN')->count();
        if($res>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
