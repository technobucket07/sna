<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table='payment_details';
    protected $guarded= ['id'];
    protected $primaryKey='id';

}
