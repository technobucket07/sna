<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageFeatures extends Model
{
    use SoftDeletes;
    protected $table='packageFeatures';
    protected $guarded= ['featureId'];
    protected $primaryKey='featureId';
}
