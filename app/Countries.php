<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
    use SoftDeletes;
    protected $table='countries';
    protected $guarded= ['countryId'];
    protected $primaryKey='countryId';


    public function venues()
    {
        return $this->hasMany('App\Venue' ,'fk_countryId','countryId');
    }

}
