<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoursePath extends Model
{
    use SoftDeletes;
    protected $table='coursePath';
    protected $guarded= ['pathId'];
    protected $primaryKey='pathId';

    public static function get_path_name($id)
    {
        $name= CoursePath::where('pathId',$id)->value('pathName');
        return $name;
    }
}
