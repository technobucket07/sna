<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resources extends Model
{
    use SoftDeletes;
    protected $table='resources';
    protected $guarded= ['resourceId'];
    protected $primaryKey='resourceId';


    public static function get_resource_name($id)
    {
        $name= Resources::where('resourceId',$id)->value('resourceName');
        return $name;
    }

    public function courses() {
        return $this->belongsToMany('App\Course','courseResourceRelation','fk_resourceId','fk_courseId');
    }
}
