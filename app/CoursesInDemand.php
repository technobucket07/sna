<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoursesInDemand extends Model
{
    use SoftDeletes;
    use \Rutorika\Sortable\SortableTrait;
    protected $table='coursesInDemand';
    protected $guarded= ['demandId'];
    protected $primaryKey='demandId';

    protected static $sortableField = 'displayOrder';

    public function course() {
        return $this->belongsTo('App\Course','fk_courseID','courseId');
    }
}
