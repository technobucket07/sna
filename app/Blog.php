<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;
    protected $table='blogNews';
    protected $guarded= ['postId'];
    protected $primaryKey='postId';


    public static function get_blognews_name($id)
    {
        $name= Blog::where('postId',$id)->value('postTitle');
        return $name;
    }
}
