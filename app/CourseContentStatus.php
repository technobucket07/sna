<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseContentStatus extends Model
{
    use SoftDeletes;
    protected $table='courseContentStatus';
    protected $guarded= ['id'];
    protected $primaryKey='id';


    public function courseContent() {
        return $this->belongsToMany('App\CourseRawDetail','courseContentStatusRelation','fk_statusId','fk_contentId');
    }
}
