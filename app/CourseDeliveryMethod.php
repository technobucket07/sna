<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseDeliveryMethod extends Model
{
    use SoftDeletes;
    protected $table='courseDeliveryMethod';
    protected $guarded= ['methodId'];
    protected $primaryKey='methodId';

    public function courses() {
        return $this->belongsToMany('App\Course','courseDeliveryMethodRelation','fk_methodId','fk_courseId');
    }
}
