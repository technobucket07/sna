<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseBulletPoint extends Model
{
    use SoftDeletes;
    use \Rutorika\Sortable\SortableTrait;
    protected $table='courseBulletPoint';
    protected $guarded= ['bulletPointId'];
    protected $primaryKey='bulletPointId';

    protected static $sortableField = 'bulletPointSortOrder';

    public function course() {
    	return $this->belongsTo('App\Course');
    }


    public static function get_bullet_name($id)
    {
        $name= CourseBulletPoint::where('bulletPointId',$id)->value('bulletPointText');
        return $name;
    }
}
