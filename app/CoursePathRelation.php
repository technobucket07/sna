<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoursePathRelation extends Model
{
    use SoftDeletes;
    protected $table='coursePathRelation';
    protected $guarded= ['pathRelationId'];
    protected $primaryKey='pathRelationId';

    public static function getPathCourses($pId)
    {
        $result = CoursePathRelation::where('fk_pathId',$pId)->get();

        //dd($result);

        return $result;
    }

}
