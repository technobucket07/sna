<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
	use SoftDeletes;
    protected $table='orders';
    protected $guarded= ['OrderID'];
    protected $primaryKey='OrderID';
}
