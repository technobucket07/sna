<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseWhatsIncluded extends Model
{
    use SoftDeletes;
    protected $table='courseWhatsIncluded';
    protected $guarded= ['whatsIncludedId'];
    protected $primaryKey='whatsIncludedId';

    public function courses() {
        //return $this->hasMany('App\CourseWhatsIncludedRelation','fk_whatsIncludedId');
        return $this->belongsToMany('App\Course','courseWhatsIncludedRelation','fk_whatsIncludedId','fk_courseId');
    }

    public static function get_whatsinc_name($id)
    {
       $name= CourseWhatsIncluded::where('whatsIncludedId',$id)->value('whatsIncludedText');
        return $name;
    }
}
