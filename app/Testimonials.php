<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonials extends Model
{
    use SoftDeletes;
    protected $table='testimonials';
    protected $guarded= ['testimonialId'];
    protected $primaryKey='testimonialId';


    public static function get_testimonial_name($id)
    {
        $name= Testimonials::where('testimonialId',$id)->value('customer');
        return $name;
    }
}
