<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer(
            'admin/shared/adminMaster',
            'App\Http\ViewComposers\MasterPageComposer'
        );

        view()->composer(
            'admin/login',
            'App\Http\ViewComposers\MasterPageComposer'
        );
    }
}
