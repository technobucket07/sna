<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseReview extends Model
{
    use SoftDeletes;
    protected $table='courseReview';
    protected $guarded= ['reviewId'];
    protected $primaryKey='reviewId';

    public function course() {
    	return $this->belongsTo('App\Course');
    }

    public static function get_review_name($id)
    {
        $name= CourseReview::where('reviewId',$id)->value('reviewTitle');
        return $name;
    }
}
