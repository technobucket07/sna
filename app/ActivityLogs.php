<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityLogs extends Model
{
    use SoftDeletes;
    protected $table='activity_log';
    protected $guarded= ['id'];
    protected $primaryKey='id';
}
