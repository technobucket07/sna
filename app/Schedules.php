<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedules extends Model
{
    use SoftDeletes;
    protected $table='schedules';
    protected $guarded= ['scheduleId'];
    protected $primaryKey='scheduleId';

    public function course() {
    	return $this->belongsTo('App\Course');
    }

    public function venue() {
    	return $this->belongsTo('App\Venue','fk_venueId','venueId');
    }

    public static function courseSchedules($fk_courseId='',$fk_venueId='', $popularChildCourseIds='') {

        $query = Self::leftjoin('venue','fk_venueId','=','venueId')
                    ->where('fk_venueId','<>',0)
                    ->where(function($query) use ($fk_courseId) {
                        if(!empty($fk_courseId)) {
                            $query->where('fk_courseId',$fk_courseId);
                        }
                    })
                    ->where(function($query) use ($fk_venueId)  {
                        if(!empty($fk_venueId)) {
                            $query->where('fk_venueId', $fk_venueId);
                        }
                    })
                    ->where(function($query) use ($popularChildCourseIds){
                        if(!empty($popularChildCourseIds)) {
                            $query->whereIn('fk_courseId', $popularChildCourseIds);
                        }
                    })
                    ->whereDate('responseDate','>=', date('Y-m-d'))
                    ->orderByRaw(\DB::raw('venue.displayOrder asc'))
                    ->orderBy('responseDate')->paginate(10);    

        return $query;
    }

    public static function courseTopicSchedules($fk_venueId='', $popularChildCourseIds='') {

        $query = Self::leftjoin('venue','fk_venueId','=','venueId')
                    ->where('fk_venueId','<>',0)
                    ->whereIn('fk_courseId', $popularChildCourseIds)
                    ->where(function($query) use ($fk_venueId)  {
                        if(!empty($fk_venueId)) {
                            $query->where('fk_venueId', $fk_venueId);
                        }
                    })
                    ->whereDate('responseDate','>=', date('Y-m-d'))
                    ->orderByRaw(\DB::raw('venue.displayOrder asc'))
                    ->orderBy('responseDate')->paginate(10);    

        return $query;
    }
    
}
