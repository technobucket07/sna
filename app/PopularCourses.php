<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PopularCourses extends Model
{
    use SoftDeletes;
    use \Rutorika\Sortable\SortableTrait;
    protected $table='popularcourses';
    protected $guarded= ['popularCourseID'];
    protected $primaryKey='popularCourseID';

    protected static $sortableField = 'displayOrder';

    public function course() {
        return $this->belongsTo('App\Course','fk_courseID','courseId');
    }
}
