<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueFeature extends Model
{
    protected $table='venueFeatures';
    protected $guarded= ['id'];
    protected $primaryKey='id';
}
