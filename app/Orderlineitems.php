<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orderlineitems extends Model
{
	use SoftDeletes;
    protected $table='orderlineitems';
    protected $guarded= ['LineID'];
    protected $primaryKey='LineID';
}
