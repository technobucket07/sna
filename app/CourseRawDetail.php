<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseRawDetail extends Model
{
    use SoftDeletes;
    protected $table='courseRawDetail';
    protected $guarded= ['id'];
    protected $primaryKey='id';


    public function courseContentStatus() {
        return $this->belongsToMany('App\CourseContentStatus','courseContentStatusRelation','fk_contentId','fk_statusId')->withPivot('updatedBy');
    }
}
