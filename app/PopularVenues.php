<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PopularVenues extends Model
{
    use SoftDeletes;
    use \Rutorika\Sortable\SortableTrait;
    protected $table='popularlocations';
    protected $guarded= ['popularLocationID'];
    protected $primaryKey='popularLocationID';

    protected static $sortableField = 'displayOrder';

    public function venue() {
        return $this->hasOne('App\Venue','venueId','fk_venueID');
    }
}
