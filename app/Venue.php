<?php

namespace App;

use App\Scopes\VenueOrderScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Venue extends Model
{
    use SoftDeletes;
    use \Rutorika\Sortable\SortableTrait;
    protected $table='venue';
    protected $guarded= ['venueId'];
    protected $primaryKey='venueId';

    protected static $sortableField = 'displayOrder';

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new VenueOrderScope);
    }

    public function country()
    {
        return $this->belongsTo('App\Countries','fk_countryId','countryId');
    }

   	public function popularVenues() {
        return PopularVenues::all();
    }


    public static function get_Venue_Name($cId){

        $venueName = Venue::where('venueId', $cId)->value('venueName');
        return $venueName;

    }


    public static function is_Popular($cId)
    {
        $res = DB::table('popularlocations')->where('fk_venueID',$cId)->where('fk_countryID','GB')->count();
        if($res>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function schedules() {
        return $this->hasMany('App\Schedules','fk_venueID');
    }
}
