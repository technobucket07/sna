<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ordercardtypes extends Model
{
	use SoftDeletes;
    protected $table='ordercardtypes';
    protected $guarded= ['CardTypeID'];
    protected $primaryKey='CardTypeID';
}
