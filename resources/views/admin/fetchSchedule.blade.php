@extends('admin.shared.adminMaster')


@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Fetch Schedule
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Schedules</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<style>
    .ui-datepicker-calendar {
        display: none;
    }

    .ui-datepicker-month{
        display: none;
    }

    .ui-datepicker-year{
        width: 90% !important;
        height: 30px !important;
        padding-left: 10px;
    }
    .ui-datepicker-prev, .ui-datepicker-next{
       display: none;
    }
</style>

    <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
            @endif
    </div>

    <div class="row">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Fetch Schedules</h3>
            </div>
            <div class="box-body">
                {{Form::open(['url' => '/admin/fetchschedule'])}}
                <div class="form-group">


                    <div class="form-group">
                        <label for="content">Select Country</label>
                        {{ Form::select('CountryID', $countries ,'GB' ,['class' => 'form-control']) }}

                    </div>

                    <div class="form-group">
                        <label>Year</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right monthYearPicker" name="EventYear">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="col-md-12">
                        {{Form::button('Fetch Schedules',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}
                    </div>


                </div>
                {{Form::close()}}
            </div>
        </div>

    </div>
</section>
<!-- /.content -->

@endsection

@section('addonjquery')
    <script>
        $(function() {
            $('.monthYearPicker').datepicker({

                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'yy'
            }).focus(function() {
                var thisCalendar = $(this);
                $('.ui-datepicker-calendar').detach();
                $('.ui-datepicker-close').click(function() {
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    thisCalendar.datepicker('setDate', new Date(year));
                });
            });
        });
    </script>
    @endsection