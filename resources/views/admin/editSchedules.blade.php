@extends('admin.shared.adminMaster')


@section('content')

    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>

        </h1>
        <ol class="breadcrumb">
            <li ><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li ><a href="/admin/listschedules">Schedule</a></li>
            <li class="active">Edit Schedule</li>
        </ol>
        <br/>

    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Schedule</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {{Form::open(['url' => '/admin/schedule/update/'.$scheduleDetail['scheduleId']])}}
                    <div class="box-body">

                        <div class="form-group">
                            <label for="content"> Country</label>
                            {{ Form::select('fk_countryId', $countries ,$scheduleDetail['fk_countryId'] ,['class' => 'form-control select2', 'id'=>'country', 'data-placeholder'=>'Select Country']) }}
                        </div>
                        <div class="form-group">
                            <label for="content"> Location</label>
                            {{ Form::select('fk_locationId', $locations ,$scheduleDetail['fk_locationId'] ,['class' => 'form-control select2', 'id'=>'locations', 'data-placeholder'=>'Select Location']) }}
                        </div>

                        <div class="form-group">
                            <label for="responseDate">Event Date</label>
                            {{Form::date('responseDate', $scheduleDetail['responseDate'] ,array_merge(['class' => ' form-control']))}}
                        </div>
                        <div class="form-group">
                            <label for="responsePrice">Event Price</label>
                            {{Form::number('eventPrice', $scheduleDetail['eventPrice'] ,array_merge(['class' => ' form-control']))}}
                        </div>

                        <div class="form-group">
                            <label for="courseDuration">Course Duration</label>
                            {{Form::text('duration',  $scheduleDetail['duration'] ,array_merge(['class' => 'form-control']))}}
                            {{Form::hidden('courseId',$scheduleDetail['fk_courseId'])}}
                        </div>




                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::button('Update Schedule',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}

                    </div>
                    {{Form::close()}}
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->

@endsection

@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script src="{{URL('/js/location.js')}}"></script>
    <script>
        $('.select2').select2();

        var $venues = $('#Location').select2();


        $venues.val([

            '{{$scheduleDetail['fk_venueId']}}',

        ]).trigger("change");


        $(":input[type=text]").each(function(){
            var words = $.trim($(this).val()).split(' ');
            var Characters = $(this).val().length;
            if($(this).next('span').length == 0)
            {
                $(this).after('<span></span>');
            }
            $(this).next('span').text('Words: '+words.length+' , Characters: '+Characters);
        });

        $('input[type=text]').keyup(function(){
            var words = $.trim($(this).val()).split(' ');
            var Characters = $(this).val().length;
            if($(this).next('span').length == 0)
            {
                $(this).after('<span></span>');
            }
            $(this).next('span').text('Words: '+words.length+' , Characters: '+Characters);

        });


    </script>
@endsection