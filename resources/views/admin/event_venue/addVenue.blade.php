@extends('admin.shared.adminMaster')
@section('content')
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
    <link rel="stylesheet" href="{{Url('admin-css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Venues
        </h1>
        <ol class="breadcrumb">
            <li ><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Venue</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
            @endif
            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Venue</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {{Form::model($venueDetails,['url' => 'admin/venue/details/submit','files'=>'true'])}}
                    {{Form::hidden('id', $value=null)}}
                    <fieldset class="gllpLatlonPicker">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="content">Venue Name</label>
                                {{Form::text('venueName', $value=null ,array_merge(['class' => 'gllpSearchField form-control']))}}
                            </div>
                            <div class="form-group">
                                <label for="content">Venue Address</label>
                                {{Form::text('address', $value=null ,array_merge(['class' => 'form-control']))}}
                            </div>
                            <div class="form-group">
                                <label for="content">Venue Country</label>
                                {{ Form::select('fk_countryId', $countries ,$value=null ,['class' => 'form-control select2', 'id'=>'country', 'data-placeholder'=>'Select Country']) }}
                            </div>
                            <div class="form-group">
                                <label for="content">Venue Location</label>
                                {{ Form::select('location', $locations ,$value=null ,['class' => 'form-control select2', 'id'=>'locations', 'data-placeholder'=>'Select Location']) }}
                            </div>
                            <div class="form-group">
                                <label for="content">Features</label>
                                {{ Form::select('fk_featureId[]', $features ,$selectedFeatures ,['class' => 'form-control select2', 'multiple'=>'multiple', 'data-placeholder'=>'Features']) }}
                            </div>
                            <div class="form-group">
                                <label for="content">Venue Phone</label>
                                {{Form::text('phone', $value=null ,array_merge(['class' => 'form-control']))}}
                            </div>
                            <div class="form-group">
                                <label for="content">Venue Email</label>
                                {{Form::text('email', $value=null ,array_merge(['class' => 'form-control']))}}
                            </div>
                            <div class="form-group">
                                <label for="VenueImage">Venue Image</label>
                                @if($picturePath != '')
                                    <img src="{{url($picturePath)}}" height="200px" width="150px">
                                @endif
                                {{Form::file('image')}}
                                {{Form::hidden('image', $value=null)}}
                            </div>

                            <div class="form-group">
                                <label for="venueDescription">Venue Description</label>
                                {{Form::textarea('venueDescription', $value=null ,array_merge(['class' => 'textarea form-control']))}}

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            {{Form::button('Submit Details',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}
                        </div>
                    </fieldset>
                    {{Form::close()}}
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->
    @include('mceImageUpload::upload_form')
@endsection

@section('addonjquery')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=cw3o8zabt4oa1k0g76sr3rhccj45itc1jecapcboi24mcu6y"></script>
    <script src="{{Url('admin-css/tinymce-charactercount.plugin.js')}}"></script>
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script>
        $('.select2').select2();

        tinymce.init({ selector:'textarea',
            height:300,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code',
                    'wordcount',
                    'charactercount'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            relative_urls: false,
            file_browser_callback: function(field_name, url, type, win) {
                // trigger file upload form
                if (type == 'image') $('#formUpload input').click();
            }
        });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADlk166150RMLLGby78Ayq9kUKyAdHtp0"></script>
    <script src="{{URL('/js/jquery-gmaps-latlon-picker.js?lnkn')}}"></script>
    <script src="{{URL('/js/location.js')}}"></script>

    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
          //  $(".textarea").wysihtml5();

            $('input[type=text]').keyup(function(){
                var words = $.trim($(this).val()).split(' ');
                var Characters = $(this).val().length;
                if($(this).next('span').length == 0)
                {
                    $(this).after('<span></span>');
                }
                $(this).next('span').text('Words: '+words.length+' , Characters: '+Characters);

            });
        });

    </script>
@endsection
