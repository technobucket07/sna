@extends('admin.shared.adminMaster')


@section('content')
    <style>
        .sortable-handle:hover{
            cursor: all-scroll;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Venue List &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="{{url('admin/venue/details')}}" class="btn bg-olive">Add New Venue</a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Venue</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Venue List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Venue Name</th>
                        <th>Country</th>
                        <th>Location</th>
                        <th>Admin</th>
                    </tr>
                    </thead>
                    <tbody class="sortable" data-entityname="venues">

                    @foreach($venueList as $item  )

                        <tr data-itemId="{{$item['id']}}">
                            <td> {{$item['venueName']}}</td>
                            <td> {{getCountryAttrUsingId($item['fk_countryId'], 'name')}}</td>
                            <td> {{getLocationAttrUsingId($item['location'], 'name')}}</td>
                            <td>
                                 <a href="{{url('admin/venue/details/'.$item['id'])}}" class="btn btn-warning">Edit</a>
                                 <a href="{{url('admin/venue/delete/'.$item['id'])}}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger" >Delete</a>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('addonjquery')
    <script>
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50, 100,"All"]]
        });
    </script>
@endsection
