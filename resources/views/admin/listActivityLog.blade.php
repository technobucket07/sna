@extends('admin.shared.adminMaster')


@section('content')

    <section class="content-header">
        <h1 class="text-capitalize"> Activity Log</h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/admin/courses"> Courses</a></li>
            <li class="active">Activity List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-capitalize">Activity Log</h3>
                </div>


                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Activity</th>
                            <th>By User</th>
                            <th>Date Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $item)
                            <tr>
                                <td> {{ $item['description'] }}</td>
                                <td> <?php $s = \App\User::find($item['causer_id']); echo $s['name'] ?> </td>
                                <td>{{ $item['created_at'] }}</td>

                            </tr>
                        @endforeach


                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection


@section('addonjquery')
    <script>
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50, 100,"All"]]
        });
    </script>

@endsection
