@extends('admin.shared.adminMaster')
@section('content')
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">@if(isset($userDetais['name']) ) {{$userDetais['name']}}@else Add User @endif</h3>
                            @if(isset($userDetais['name']))
                                <div class="box-tools">
                                    <a href="{{url('admin/user/list')}}" class="btn btn-success btn-xs">Add New User</a>
                                </div>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            {{Form::model($userDetais, ['url' => 'admin/user/details/submit'])}}
                            {{Form::hidden('id', $value=null)}}
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="content">Name</label>
                                                {{Form::text('name', $value = null, array('class'=>'form-control'))}}
                                            </div>
                                            <div class="form-group">
                                                <label for="content">Email</label>
                                                {{Form::text('email', $value = null, array('class'=>'form-control'))}}
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Name</label>
                                                {{ Form::select('roles[]', $roles ,$rolesAssigned ,['class' => 'form-control select2', 'data-placeholder'=>'Select Roles', 'multiple'=>'multiple']) }}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                            {{Form::close()}}
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>

            </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> User List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="userList" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Roles</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($userList as $user  )
                                    <tr>
                                        <td>
                                            {{$user['name']}}
                                        </td>
                                        <td> {{$user['email']}}</td>
                                        <td> <ul>@foreach($user->getRoleNames() as $role)<li>{{$role}}</li>@endforeach</ul></td>
                                        <td><a href="{{url('admin/user/list?id='.$user['id'])}}" class="btn btn-warning">Edit</a></td>
                                        <td>
                                            <a  class="btn btn-danger" onclick = "confirmDelete('{{url('admin/user/delete?id='.$user['id'])}}')">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
<script>
    $('.select2').select2();

    $(function () {
        $('#userList').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
    });

</script>
@endsection
