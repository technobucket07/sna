@extends('admin.shared.adminMaster')


@section('content')
    <link rel="stylesheet" href="{{Url('admin-css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Change Password
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Change Password</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">

            @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
            @endif

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> Change Password</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {{Form::open(['url' => '/admin/changepassword/update'])}}
                    <div class="box-body">

                        <div class="form-group">
                            <label for="pass">New Password</label>
                            {{Form::password('password', array_merge(['class' => 'form-control']))}}
                        </div>

                        <div class="form-group">
                            <label for="cpass">Confirm Password</label>
                            {{Form::password('confirm_password',array_merge(['class' => 'form-control']))}}
                        </div>


                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::button('Update Password',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}

                    </div>
                    {{Form::close()}}

                </div>
                <!-- /.box -->
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
