@extends('admin.shared.adminMaster')


@section('content')
    <link rel="stylesheet" href="{{Url('admin-css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <link rel="stylesheet" href="{{url('admin-css/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{url('admin-css/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Event Calender
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Event Calender</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h4 class="box-title">Event Status</h4>
                    </div>
                    <div class="box-body">
                        <!-- the events -->
                        <div id="external-events">
                            <div class="alert text-bold bg-green">Confirmed</div>
                            <div class="alert text-bold bg-yellow">In Progress</div>
                            <div class="alert text-bold bg-red">Pending</div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-9">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection

@section('addonjquery')
    <script src="{{Url('admin-css/bower_components/moment/moment.js')}}"></script>
    <script src="{{Url('admin-css/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
    <script>
        $(function () {
            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date()
            var d    = date.getDate(),
                m    = date.getMonth(),
                y    = date.getFullYear()
            $('#calendar').fullCalendar({
                displayEventTime: false,
                header    : {
                    left  : 'prev,next today',
                    center: 'title',
                   // right : 'month,agendaWeek,agendaDay'
                    right : 'month'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week : 'week',
                    day  : 'day'
                },
                //Random default events
                events    : [
                    {!! $str !!}

                ],
                eventMouseover: function(calEvent, jsEvent) {
                    var tooltip = '<div class="tooltipevent" style="min-width: 200px;background:#2d363ae8;color: #fff;padding: 10px;border-radius: 20px ;position:absolute;z-index:10001;"><p style="text-align: center;">'+calEvent.dat+'</p><p style="text-decoration: underline;text-decoration-color: #00acd6">' + calEvent.title
                        + '</p><p>Locations:</p><ul>'+calEvent.loc+'</ul></div>';
                    var $tooltip = $(tooltip).appendTo('body');

                    $(this).mouseover(function(e) {
                        $(this).css('z-index', 10000);
                        $tooltip.fadeIn('500');
                        $tooltip.fadeTo('10', 1.9);
                    }).mousemove(function(e) {
                        $tooltip.css('top', e.pageY + 10);
                        $tooltip.css('left', e.pageX + 20);
                    });
                },
                eventMouseout: function(calEvent, jsEvent) {
                    $(this).css('z-index', 8);
                    $('.tooltipevent').remove();
                },
                editable  : false,
                droppable : false, // this allows things to be dropped onto the calendar !!!

            })
        })
    </script>
    @endsection
