@extends('admin.shared.adminMaster')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Instructor
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Instructor</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> Instructors List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="instList" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($instructorList as $instructor  )
                                    <tr>
                                        <td>
                                            {{$instructor['first_name']}} {{$instructor['first_name']}}
                                        </td>
                                        <td> {{$instructor['email']}}</td>
                                        <td>{{$instructor['phone']}}</td>
                                        <td><a href="{{url('admin/instructor/details/'.$instructor['id'])}}" class="btn btn-warning">Edit</a></td>
                                        <td>
                                            <a  class="btn btn-danger" onclick = "confirmDelete('{{url('admin/instructor/delete/'.$instructor['id'])}}')">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination pull-right">
                            {{$instructorList->links()}}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
<script>
    $(function () {
        $('#instList').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
    });

</script>
@endsection
