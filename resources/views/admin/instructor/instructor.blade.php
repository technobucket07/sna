@extends('admin.shared.adminMaster')
@section('content')
        <style>
            .profileupload {
                margin: 0px 0px 20px 0px;
            }

            .no-padding {
                padding: 0 !important;
            }
            .btn-bs-file {
                position: relative;
            }

            .btn-success {
                background-color: #00a65a;
                border-color: #008d4c;
            }
            .profileupload input[type=file] {
                width: 100%;
            }


        </style>
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Instructor
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Instructor</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif

    <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add Instructor</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {{Form::model($instructorDetails, ['url' => 'admin/instructor/details/submit', 'files' => true])}}
                    {{Form::hidden('id', $value=null)}}

                        <div class="box-body">
                            <div class="col-md-3 text-center">
                                <div class="form-group">
                                   <img src="{{url($picturePath)}}" width="150px"  height="150px">
                                </div>
                                <div class="form-group  text-center">
                                    <div class="no-padding profileupload text-center">
                                        <input value="Choose Picture" accept="image/*" type="file" name="image">
                                        {{Form::hidden('image', $value=null)}}
                                    </div>
                                </div>
                                <div class="form-group  text-center">
                                    <div class="no-padding text-center">
                                        <label>Choose CV
                                            <input value="Choose CV" type="file" name="CV">
                                            {{Form::hidden('CV', $value=null)}}
                                        </label>
                                    </div>
                                    @if($cvPath != '')
                                        <a href="{{url($cvPath)}}" target="_blank" class="btn btn-success btn-xs"> View CV</a>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            {{ Form::radio('gender', 'Male') }} Male
                                        </label>
                                        <label>
                                            {{ Form::radio('gender', 'Female') }}
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">First Name</label>
                                            {{Form::text('first_name', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Last Name</label>
                                            {{Form::text('last_name', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Email</label>
                                            {{Form::text('email', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Phone</label>
                                            {{Form::text('phone', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">LinkedIn Profile</label>
                                            {{Form::text('linkedIn_profile', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Skills</label>
                                            {{Form::textarea('skills', $value = null, array('class'=>'form-control', 'rows'=>3))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Certifications</label>
                                            {{Form::textarea('certifications', $value = null, array('class'=>'form-control', 'rows'=>3))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    {{Form::close()}}
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>
</section>
<!-- /.content -->
@endsection
@section('addonjquery')

@endsection