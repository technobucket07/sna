@extends('admin.shared.adminMaster')
@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Location
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Location</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"> Locations List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="locationList" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Location</th>
                            <th>Country</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($locationList as $location  )
                            <tr>
                                <td>{{$location['name']}} </td>
                                <td> {{getCountryAttrUsingId($location['fk_countryId'], 'name')}}</td>
                               <td><a href="{{url('admin/location/details/'.$location['id'])}}" class="btn btn-warning">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                        {{$locationList->links()}}
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>
</section>
<!-- /.content -->
@endsection
@section('addonjquery')
    <script>
        $(function () {
            $('#locationList').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
        });

    </script>
@endsection
