@extends('admin.shared.adminMaster')
@section('content')
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Location
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Location</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Location</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {{Form::model($locationDetails, ['url' => 'admin/location/details/submit'])}}
                        {{Form::hidden('id', $value=null)}}
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="content">Select Country</label>
                                            {{ Form::select('fk_countryId', $countries ,$value=null ,['class' => 'form-control select2', 'data-placeholder'=>'Select Country']) }}
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            {{Form::text('name', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                        {{Form::close()}}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script>
        $('.select2').select2();
    </script>
@endsection