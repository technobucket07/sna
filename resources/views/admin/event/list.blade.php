@extends('admin.shared.adminMaster')
@section('content')
<section class="content-header">
    <h1>
        Events
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Events</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Event List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="eventList" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ScheduleId</th>
                            <th>Event Date</th>
                            <th>Course</th>
                            <th>Status</th>
                            <th>Event Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event  )
                            <tr>
                                <td>
                                    {{generateEventNumber($event['scheduleId'])}}
                                </td>
                                <td> {{$event['responseDate']}}</td>
                                <td>{{getCourseAttrUsingId($event['fk_courseId'], 'courseName') }}</td>
                                <td>
                                    @if($event['fk_instructorIds'] == '')
                                        <i class="fa fa-user text-black"></i>
                                    @else
                                        <i class="fa fa-user text-green"></i>
                                    @endif
                                    &nbsp;&nbsp;
                                    @if($event['fk_venueId'] == '')
                                        <i class="fa fa-map-marker text-black"></i>
                                    @else
                                        <i class="fa fa-map-marker text-green"></i>
                                    @endif

                                </td>
                                <td><a href="{{url('admin/event/details/'.$event['scheduleId'])}}" target="_blank" class="btn btn-warning">Event Details</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                        {{$events->links()}}
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>
</section>
<!-- /.content -->
@endsection
@section('addonjquery')
    <script>
        $(function () {
            $('#eventList').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
        });

    </script>
@endsection
