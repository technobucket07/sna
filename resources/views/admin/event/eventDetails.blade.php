@extends('admin.shared.adminMaster')
@section('content')
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
    <style>
        .event-text .nav-stacked li {
            float: left;
            width: 43%;
            margin-right: 35px;
        }
    </style>
    <section class="content-header">
        <h1>
            Events
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Events</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            @hasanyrole('event manager|venue coordinator|admin')
        @if($eventDetails['fk_venueId'] == null)
            <div class="row">
                <div class="col-sm-12">
                    <div class="callout callout-warning">
                        <h4 class="box-title">Assign Venue</h4>
                        {{Form::open(array('url'=>'admin/event/assign/venue'))}}
                            <div class="input-group margin">
                                <input type="hidden" value="{{$eventDetails['scheduleId']}}" name="scheduleId">
                                <div class="col-sm-12">
                                    {{Form::select('fk_venueId', $assignableVenues,$value=null , array('class'=>'form-control select2',  'data-placeholder'=>'Select Venue'))}}
                                </div>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-info">Assign Venue</button>
                                </div>
                                <!-- /btn-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
            @endhasanyrole

            @hasanyrole('event manager|event coordinator|admin')
            @if($eventDetails['fk_instructorIds'] == null)
            <div class="row">
                <div class="col-sm-12">
                    <div class="callout callout-warning">
                        <h4 class="box-title">Assign Instructor</h4>
                        {{Form::open(array('url'=>'admin/event/assign/instructor'))}}
                            <input type="hidden" value="{{$eventDetails['scheduleId']}}" name="scheduleId">
                            <div class="input-group margin">
                                <div class="col-sm-12">
                                    {{Form::select('fk_instructorIds[]', $assignableInstructors, $assignedInstructors, array('class'=>'form-control select2', 'multiple'=>'multiple', 'data-placeholder'=>'Select Instructor'))}}
                                </div>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-info">Assign Instructor</button>
                                </div>
                                <!-- /btn-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
            @endhasanyrole

        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="box box-primary ">
                    <div class="box-header">
                        <h3 class="box-title"> Event Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-footer no-padding event-text">
                            <ul class="nav nav-stacked">
                                <li><a><strong>Schedule Number</strong>  &nbsp;&nbsp;&nbsp;<span class="pull-right">{{generateEventNumber($eventDetails['scheduleId'])}}</span></a></li>
                                <li><a><strong>Course</strong> &nbsp;&nbsp;&nbsp;<span class="pull-right">{{getCourseAttrUsingId($eventDetails['fk_courseId'], 'courseName')}}</span></a></li>
                                <li><a><strong>Start Date</strong> &nbsp;&nbsp;&nbsp;<span class="pull-right">{{Carbon\Carbon::parse($eventDetails['responseDate'])->format('d-M-Y')}}</span></a></li>
                                <li><a><strong>Duration</strong> &nbsp;&nbsp;&nbsp;<span class="pull-right">{{$eventDetails['duration']}} Day(s)</span></a></li>
                                <li><a><strong>Location</strong> &nbsp;&nbsp;&nbsp;<span class="pull-right">{{getLocationAttrUsingId($eventDetails['fk_locationId'], 'name')}}</span></a></li>
                                <li><a><strong>Country Code</strong> &nbsp;&nbsp;&nbsp;<span class="pull-right">{{getCountryAttrUsingId($eventDetails['fk_countryId'], 'name')}}</span></a></li>
                                <li><a><strong>Instructors Assigned</strong> &nbsp;&nbsp;&nbsp;<span class="pull-right">
                                            @if($eventDetails['fk_instructorIds'] != null)
                                                @php $instructorIds = explode(",",$eventDetails['fk_instructorIds']);@endphp

                                                @foreach($instructorIds as $instructorId)
                                                    {{ ucfirst(getInstructorAttrUsingId($instructorId, 'first_name'))}} {{ ucfirst(getInstructorAttrUsingId($instructorId, 'last_name'))}}
                                                    @if($loop->remaining and $instructorId == '') @endif
                                                @endforeach
                                            @endif
                                        </span></a></li>
                                <li><a><strong>Venue Assigned</strong> &nbsp;&nbsp;&nbsp;<span class="pull-right">{{ ucfirst(getVenueAttrUsingId($eventDetails['fk_venueId'], 'venueName'))}}</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- row -->
            </div>
            <div class="col-sm-12 col-md-4 ">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Timeline</h3>
                        </div>
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active" id="timeline">
                                <div class="box-body timelinediv" id="style-3">
                                    <ul class="timeline timeline-inverse">
                                        <li>
                                            <i class="fa fa-dot-circle-o bg-blue"></i>
                                            <div class="timeline-item">
                                                <div class="timeline-body">
                                                    Event generated
                                                </div>
                                            </div>
                                        </li>
                                        @if($eventDetails['fk_instructorIds'] != null)
                                            <li>
                                                <i class="fa fa-dot-circle-o bg-blue"></i>
                                                <div class="timeline-item">
                                                    <div class="timeline-body">
                                                        Instructor Assigned by {{getUserAttrUsingId($eventDetails['instructorAssignedBy'])}}
                                                    </div>
                                                </div>
                                            </li>
                                        @endif

                                            @if($eventDetails['fk_venueId'] != null)
                                                <li>
                                                    <i class="fa fa-dot-circle-o bg-blue"></i>
                                                    <div class="timeline-item">
                                                        <div class="timeline-body">
                                                            Venue Assigned by {{getUserAttrUsingId($eventDetails['venueAssignedBy'])}}
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                        <li><i class="fa fa-clock-o bg-gray"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
                <input type="hidden" id="scheduleId" value="{{$eventDetails['scheduleId']}}">
                @hasanyrole('event manager|event coordinator|admin')
                <div class="col-sm-6">
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Comments: Instructor</h3>
                        </div>
                        <div class="box-footer box-comments ins-min-height direct-chat-messages" id="instructorCommentList">

                        </div>
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <div class="input-group">
                                <input type="text" id="instructorComment" placeholder="Comment " class="form-control userComment">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary btn-flat" onclick="storeComment('event','{{$eventDetails['scheduleId']}}', 'instructor')">Add</button>
                                        </span>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                @endhasanyrole

                @hasanyrole('event manager|Venue coordinator|admin')
                <div class="col-sm-6 ">
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Comments: Venue</h3>
                        </div>
                        <div class="box-footer box-comments ins-min-height direct-chat-messages" id="venueCommentList">

                        </div>
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <div class="input-group">
                                <input type="text" id="venueComment" placeholder="Comment " class="form-control userComment">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary btn-flat" onclick="storeComment('event','{{$eventDetails['scheduleId']}}', 'venue')">Add</button>
                                       </span>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                @endhasanyrole

                <!--<div class="col-sm-4 ">
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Delegates</h3>
                        </div>
                        <div class="box-footer box-comments ins-min-height direct-chat-messages" id="delegates">
                            <ul>
                                <li>a</li>
                                <li>b</li>
                            </ul>
                        </div>
                    </div>
                </div>-->
            </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script src="{{URL('/js/commonComments.js')}}"></script>
    <script>
        $('.select2').select2();
        var entity_id  = $('#scheduleId').val();
        getAllComments('event',entity_id,'instructor')
        getAllComments('event',entity_id,'venue')

        $(function() {
            $('#instList').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
        });
    </script>
@endsection
