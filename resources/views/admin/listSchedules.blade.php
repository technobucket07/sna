@extends('admin.shared.adminMaster')


@section('content')
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Schedule List
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Schedules</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif

    <div class="row">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Schedule List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                {{Form::open(['url' => '/admin/schedules/listbycourse'])}}
                <div class="form-group">
                    <label for="courseDeliveryMethod">Select Course</label>
                    <select id="coursess" name="fk_courseId" class="js-example-placeholder-multiple form-control">
                        @foreach($result as $item)
                            <option value="{{$item['courseId']}}">{{$item['courseDisplayName']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{Form::button('List Schedule',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}
                </div>

                {{Form::close()}}


                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Location</th>
                        <th>Date</th>
                        <th>Price</th>
                        <th>Duration</th>
                        <th>Admin</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($listschedule))
                    @foreach($listschedule as $item  )

                        <tr>
                            <td>@if($item['fk_locationId']==0)
                                {{$item['responseLocation']}} (Location Not Available Yet)
                                @else
                                    {{getLocationAttrUsingId($item['fk_locationId'], 'name')}}
                                @endif
                            </td>
                            <td><span class="hidden">{{Carbon\Carbon::parse($item['responseDate'])->format('Ymd')}}</span> {{$item['responseDate']}}</td>
                            <td>{{$item['eventPrice']}}</td>
                            <td>{{$item['duration']}}</td>
                            <td><div class="btn-group">
                                    <a href="/admin/schedule/edit/{{$item['scheduleId']}}" class="btn btn-warning">Edit</a>
                                    <a href="/admin/schedule/delete/{{$item['scheduleId']}}" class="btn btn-danger">Delete</a>
                                </div></td>
                        </tr>

                    @endforeach
                        @endif

                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
<!-- /.content -->

@endsection


@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script>

    var $coursess = $('#coursess').select2();

    @if(!empty($cid))
    $coursess.val([

        '{{$cid}}',

    ]).trigger("change");
        @endif

    </script>

@endsection