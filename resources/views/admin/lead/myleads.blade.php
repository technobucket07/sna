@extends('admin.shared.adminMaster')
@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Enquiries
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Enquiry</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"> Enquiry List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="enqList" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Assign To</th>
                            <th>Follow Up Taken</th>
                            <th>Follow Up Taken By</th>
                            <th>Is Interested</th>
                            <th>Interest Marked By</th>
                            <th>Is Converted</th>
                            <th>Converted By</th>
                            <th>Comments</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($enquiries as $enquiry )
                            <tr>
                                <td>
                                    {{$enquiry['name']}}
                                </td>
                                <td>{{$enquiry['phone']}}</td>
                                <td>
                                    @if($enquiry['assignedTo'] > 0)
                                        {{getUserAttrUsingId($enquiry['assignedTo'])}}
                                    @else
                                        {{Form::open(array('url'=>'admin/lead/assign'))}}
                                        {{Form::hidden('enquiryId', $enquiry['enquiryId'])}}
                                        <input type="submit" value = "Assign Me" class="btn btn-success">
                                        {{Form::close()}}
                                    @endif
                                </td>

                                <td>
                                    @if($enquiry['assignedTo'] > 0)
                                        @if($enquiry['followedUpBy'] > 0)
                                            @if($enquiry['isFollowedUp'] == 1)
                                                <i class="fa fa-check text-green"></i>
                                            @endif
                                        @else
                                            {{Form::open(array('url'=>'admin/lead/followed_up'))}}
                                            {{Form::hidden('enquiryId', $enquiry['enquiryId'])}}
                                            <input type="submit" name= "Yes" value = "Yes" class="btn btn-success btn-xs">
                                            {{Form::close()}}
                                        @endif

                                    @endif
                                </td>
                                <td>
                                    @if($enquiry['followedUpBy'] > 0)
                                        {{getUserAttrUsingId($enquiry['followedUpBy'])}}
                                    @endif
                                </td>

                                <td>
                                    @if($enquiry['followedUpBy'] > 0)
                                        @if($enquiry['interestMarkedBy'] > 0)
                                            @if($enquiry['isInterested'] == 1)
                                                <i class="fa fa-check text-green"></i>
                                            @else
                                                <i class="fa fa-times text-red"></i>
                                            @endif
                                        @else
                                            {{Form::open(array('url'=>'admin/lead/mark/interest'))}}
                                            {{Form::hidden('enquiryId', $enquiry['enquiryId'])}}
                                            <input type="submit" name="Yes" value = "Yes" class="btn btn-success btn-xs">
                                            <input type="submit" name="No" value = "No" class="btn btn-danger btn-xs">
                                            {{Form::close()}}
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($enquiry['interestMarkedBy'] > 0)
                                        {{getUserAttrUsingId($enquiry['interestMarkedBy'])}}
                                    @endif
                                </td>
                                <td>
                                    @if($enquiry['interestMarkedBy'] > 0)
                                        @if($enquiry['convertedBy'] > 0)
                                            @if($enquiry['isConverted'] == 1)
                                                <i class="fa fa-check text-green"></i>
                                            @else
                                                <i class="fa fa-times text-red"></i>
                                            @endif
                                        @else
                                            {{Form::open(array('url'=>'admin/lead/mark/converted'))}}
                                            {{Form::hidden('enquiryId', $enquiry['enquiryId'])}}
                                            <input type="submit" name="Yes" value = "Yes" class="btn btn-success btn-xs">
                                            <input type="submit" name="No" value = "No" class="btn btn-danger btn-xs">
                                            {{Form::close()}}
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($enquiry['convertedBy'] > 0)
                                        {{getUserAttrUsingId($enquiry['convertedBy'])}}
                                    @endif
                                </td>
                                <td class="text-center"><i class="fa fa-commenting-o cursor" onclick="openCommentModalBox('{{$enquiry['enquiryId']}}')"></i></td>

                                <td><a href="{{url('admin/lead/details/'.$enquiry['enquiryId'])}}" class="btn btn-warning btn-xs">Edit</a></td>
                                <td>
                                    <a  class="btn btn-danger btn-xs" onclick = "confirmDelete('{{url('admin/lead/delete/'.$enquiry['enquiryId'])}}')">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                        {{$enquiries->links()}}
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        @include('admin.lead.fragments.comments')
    </div>
</section>
<!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('js/leadComments.js')}}"></script>
    <script>

        $(function () {
            $('#enqList').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            });
        });
    </script>
@endsection
