@extends('admin.shared.adminMaster')
@section('content')
    <style>
        .red-border{
            border : 1px solid red;
        }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Payment
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Payment List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> Payment List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="enqList" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Payment Status</th>
                                <th>Payment Details</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($convertedLeads as $convertedLead )
                                <tr>
                                    <td>{{$convertedLead['name']}}</td>
                                    <td>{{$convertedLead['phone']}}</td>
                                    <td>{{$convertedLead['email']}}</td>
                                    <td class="text-center"><i class="fa  fa-commenting-o cursor" onclick="openCommentModalBox('{{$convertedLead['enquiryId']}}')"></i></td>
                                    <td><a href="{{url('admin/lead/payment/details/'.$convertedLead['enquiryId'])}}" class="btn btn-primary">Payment Details</a></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="pagination pull-right">
                            {{$convertedLeads->links()}}
                        </div>
                    </div>
                    {{Form::close()}}<!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script>
        var URL = window.location.origin+'/';
        $(function () {
            $('#enqList').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            });
        });

    </script>
@endsection
