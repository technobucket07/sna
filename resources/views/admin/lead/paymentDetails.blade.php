@extends('admin.shared.adminMaster')
@section('content')
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
    <link href="{{url('admin-css/plugins/datepicker/datepicker3.css')}}" rel="text/stylesheet">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Payment
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Payment</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"> Payment Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {{Form::model($paymentDetails, ['url' => 'admin/lead/payment/details/submit'])}}
                        {{Form::hidden('id', $value=null)}}
                        {{Form::hidden('fk_leadId', $leadId, array('id'=>'leadId'))}}

                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Select Payment Type</label><br>
                                            {{Form::select('payment_type',$paymentTypes, $value = null, array('class'=>'form-control select2', 'id'=>'payment_type','data-placeholder'=>'Select PaymentType', 'onChange'=>'fillAmount()'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Select Payment Type</label><br>
                                            {{Form::select('currency_code',$currencies, $value = null, array('class'=>'form-control select2', 'data-placeholder'=>'Select Currency'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Total Amount</label>
                                            {{Form::number('total_amount', $value = null, array('class'=>'form-control', 'id'=>'total_amount', 'placeholder'=>'Total Amount', 'onKeyUp'=>'fillAmount()'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Amount Paid</label>
                                            {{Form::number('paid_amount', $value = null, array('class'=>'form-control','id'=>'paid_amount', 'placeholder'=>'Amount Paid'))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                        {{Form::close()}}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-4  col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"> Lead Details</h3>
                        <div class="box-tools">
                            <a href="{{url('admin/lead/details/'.$leadDetails['enquiryId'])}}" target="_blank" class="btn btn-primary btn-xs pull-right">View Lead Details</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <div class="box-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Name : </label> <strong> {{$leadDetails['name']}}</strong>
                                          </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Email : </label> <strong> {{$leadDetails['email']}}</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Phone : </label> <strong> {{$leadDetails['phone']}}</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Company : </label> <strong> {{$leadDetails['company']}}</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Course : </label> <strong> {{getCourseAttrUsingId($leadDetails['course'], 'courseName')}}</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Delegates : </label> <strong> {{$leadDetails['delegates']}}</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Address : </label> <strong> {{$leadDetails['address']}}</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Location : </label> <strong> {{$leadDetails['location']}}</strong>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-4  col-sm-12">
                  <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Comments</h3>
                        </div>
                        <div class="box-footer box-comments ins-min-height direct-chat-messages" id="CommentList">

                        </div>
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <div class="input-group">
                                <input type="text" id="Comment" placeholder="Comment " class="form-control userComment">
                                     <span class="input-group-btn">
                                     <button type="button" class="btn btn-primary btn-flat" onclick="storeComment('lead','{{$leadDetails['enquiryId']}}', '')">Add</button>
                                </span>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                  </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script src="{{URL('js/commonComments.js')}}"></script>
    <script src="{{url('admin-css/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

    <script>
        $('.select2').select2();
        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
        var entity_id  = $('#leadId').val();
        console.log(entity_id);
        getAllComments('lead',entity_id,'')

        function fillAmount(){
            var total_amount = $('#total_amount').val();
            var payment_type = $('#payment_type').val();
            if(payment_type == 'Full'){
                $('#paid_amount').val(total_amount);
                $('#paid_amount').attr('readonly', 'readonly');
            }else{
                $('#paid_amount').removeAttr('readonly');
                $('#paid_amount').val('');
            }
        }

    </script>
@endsection