@extends('admin.shared.adminMaster')
@section('content')
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
    <link href="{{url('admin-css/plugins/datepicker/datepicker3.css')}}" rel="text/stylesheet">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Leads
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Leads</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"> Lead Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {{Form::model($leadDetails, ['url' => 'admin/lead/details/submit'])}}
                        {{Form::hidden('enquiryId', $value=null)}}

                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            {{Form::text('name', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Email</label>
                                            {{Form::text('email', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Phone</label>
                                            {{Form::text('phone', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Company</label>
                                            {{Form::text('company', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Course</label><br>
                                            {{Form::select('course',$course, $value = null, array('class'=>'form-control select2'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Delegates</label>
                                            {{Form::text('delegates', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Address</label>
                                            {{Form::text('address', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Location</label>
                                            {{Form::text('location', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Date</label>
                                            {{Form::text('date', $value = null, array('class'=>'form-control date','autocomplete'=>'off' ,'data-date-format'=>'yyyy-mm-dd'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Price</label>
                                            {{Form::text('price', $value = null, array('class'=>'form-control'))}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Message</label>
                                            {{Form::textarea('message', $value = null, array('class'=>'form-control', 'rows'=>3))}}
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                        {{Form::close()}}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script src="{{url('admin-css/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

    <script>
        $('.select2').select2();
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>
@endsection