@extends('admin.shared.adminMaster')
@section('content')
    <style>
        .red-border{
            border : 1px solid red;
        }
    </style>
    <link rel="stylesheet" href="{{URL('/admin-css/select2/css/select2.css')}}">
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Leads
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Leads</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                {{Form::open(array('url'=>'admin/lead/assign'))}}
                    <div class="box-header">
                        <h3 class="box-title"> @if($assignedTo != '')Assigned @else All @endif Leads</h3>
                        <div class="box-tools">
                            @if(count($enquiries) > 0 and $assignedTo == '' )
                                <input type="submit" value="Assign To Me" class="btn btn-primary" onclick="submitAssignments()">
                            @endif
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                    <table id="enqList" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            @if($assignedTo == '')<th><label><input type="checkbox" name="entityId" id="example-select-all"></label></th>@endif
                            <th>Name</th>
                            <th>Phone</th>
                            @if($assignedTo != '')
                                <th>Assigned To</th>
                            @endif
                            @role('sales manager')
                                @if($assignedTo == '')
                                  <th>Assign To</th>
                                @else
                                  <th>Assign To Someone else</th>
                                @endif
                            @endrole
                            <th>Comments</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>

                            @foreach($enquiries as $enquiry )
                                <tr>
                                    @if($assignedTo == '')<td><label><input type="checkbox" value="{{$enquiry['enquiryId']}}" name="enquiryId[]" ></label> </td>@endif
                                    <td>
                                        {{$enquiry['name']}}
                                    </td>
                                    <td>{{$enquiry['phone']}}</td>
                                    @if($assignedTo != '')
                                        <td>{{getUserAttrUsingId($enquiry['assignedTo'])}}</td>
                                    @endif
                                    @role('sales manager')
                                       <td><i class="fa fa-mail-forward" onclick="openModalBox('{{$enquiry['enquiryId']}}')"></i></td>
                                    @endrole
                                    @role('sales manager')

                                    @endrole
                                    <td class="text-center"><i class="fa  fa-commenting-o cursor" onclick="openCommentModalBox('{{$enquiry['enquiryId']}}')"></i></td>
                                    <td><a href="{{url('admin/lead/details/'.$enquiry['enquiryId'])}}" class="btn btn-warning">Edit</a></td>
                                    <td>
                                        <a  class="btn btn-danger" onclick = "confirmDelete('{{url('admin/lead/delete/'.$enquiry['enquiryId'])}}')">Delete</a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                        {{$enquiries->links()}}
                    </div>
                </div>
                {{Form::close()}}
                <!-- /.box-body -->
            </div>

            <div id="myModal" class="modal fade" role="dialog">
                {{Form::open(array('url'=>'admin/lead/assign'))}}
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Assign Lead</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-12 form-group ">
                                <label class=" control-label">Select User</label>
                                <input type="hidden" id="modalId" value="" name="enquiryId[]">
                                {{Form::select('assignTo', $users, [], array('class'=>'form-control select2', 'data-placeholder'=>'Select User', 'style'=>'width: 538px;','required'))}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Assign</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                </form>
            </div>

            @include('admin.lead.fragments.comments')

        </div>

    </div>
</section>
<!-- /.content -->
@endsection
@section('addonjquery')
    <script src="{{URL('/admin-css/select2/js/select2.js')}}"></script>
    <script src="{{URL('js/leadComments.js')}}"></script>

    <script>
        var URL = window.location.origin+'/';
        $(function () {
            $('#enqList').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            });
        });
        $('#example-select-all').on('click', function(){
             $('input[type="checkbox"]').prop('checked', this.checked);
        });

        function submitAssignments(){
            var favorite = [];
            $.each($("input[name='enquiryId']:checked"), function(){
                favorite.push({ value:$(this).val() });
            });

        }


    </script>
@endsection
