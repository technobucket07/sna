<div class="modal fade" id="commentModalId" style=" padding-right: 17px;"  role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Comments</h4>
            </div>
            <div id="comments" class="direct-chat-messages"></div>
            <div class="box-footer">
                <div class="input-group">
                    <input type="hidden" id="entity_id" value="">
                    <input type="text" id="newComment" placeholder="Comment " class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" id="" class="btn btn-primary btn-flat" onclick="saveComment('lead')">Add</button>
                                    </span>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>