<?php

return [
    'entities' => [
        'courses' => '\App\Course',
        'bulletpoints'=> '\App\CourseBulletPoint',
        'faq'=>'\App\CourseFaq',
        'popularcourses' => '\App\PopularCourses',
        'popularvenues'=>'\App\PopularVenues',
        'venues'=>'\App\Venue',
        'indemandcourses'=>'\App\CoursesInDemand',
        // 'articles' => '\Article' for simple sorting (entityName => entityModel) or
        // 'articles' => ['entity' => '\Article', 'relation' => 'tags'] for many to many or many to many polymorphic relation sorting
    ],
];
