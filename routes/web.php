<?php

Route::group(['middleware'=>'guest'],function() {
    Route::post('login','Auth\LoginController@login');
    Route::get('login',['as'=>'login','uses'=>'Auth\LoginController@showLoginForm']);
    Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset','Auth\ForgotPasswordController@reset');
    Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm');
    Route::get('password/reset/{token}','Auth\ForgotPasswordController@showResetForm');

});
Route::post('logout',['as'=>'logout','uses'=>'Auth\LoginController@logout']);
Route::group(['middleware'=>'auth'],function() {
    /*
     * Routes to Register a new user. Only Authenticated users can register a user
    */
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('register', ['uses' => 'Auth\RegisterController@register']);
});

Route::group(['middleware'=>'auth'],function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/admin/calender', 'HomeController@showCalender')->name('showCalender');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/admin/dashboard', ['as' => 'AdminHomeRoute', 'uses' => 'Admin\AdminDashboardController@dashboard']);
    Route::get('/admin/listschedules', ['as' => 'AdminListScheduleRoute', 'uses' => 'Admin\ScheduleController@index']);
    Route::get('/admin/addschedules', ['as' => 'AdminScheduleRoute', 'uses' => 'Admin\ScheduleController@addSchedule']);
    Route::post('/admin/schedule/addscheduledata', ['as' => 'AdminScheduleAddRoute', 'uses' => 'Admin\ScheduleController@addScheduleData']);
    Route::post('/admin/schedules/listbycourse', ['as' => 'AdminScheduleByCourseRoute', 'uses' => 'Admin\ScheduleController@displaySchedules']);

    Route::get('/admin/schedule/edit/{scheduleId}', ['as' => 'AdminScheduleByCourseRoute', 'uses' => 'Admin\ScheduleController@editSchedule']);
    Route::post('/admin/schedule/update/{scheduleId}', ['as' => 'AdminScheduleByCourseRoute', 'uses' => 'Admin\ScheduleController@updateSchedule']);
    Route::get('/admin/schedule/delete/{scheduleId}', ['as' => 'AdminDeleteScheduleByCourseRoute', 'uses' => 'Admin\ScheduleController@deleteSchedule']);

    Route::get('/admin/activitylog', ['as' => 'AdminActivityLogRoute', 'uses' => 'Admin\ActivityController@index']);



    Route::get('/admin/user/list',['uses'=>'Admin\AdminUserController@userList']);
    Route::post('admin/user/details/submit',['uses'=>'Admin\AdminUserController@submitUserDetails']);
    Route::get('/admin/user/delete',['uses'=>'Admin\AdminUserController@deleteUser']);
    Route::get('/admin/changepassword',['as'=>'AdminChangePassRoute','uses'=>'Admin\AdminUserController@index']);
    Route::post('/admin/changepassword/update',['as'=>'AdminUpdateePassRoute','uses'=>'Admin\AdminUserController@updatePassword']);

    //instructor routes
    Route::get('admin/instructor/list',  'Admin\InstructorController@instructorList');
    Route::get('admin/instructor/delete/{id}',  'Admin\InstructorController@deleteInstructor');
    Route::get('admin/instructor/details',  'Admin\InstructorController@details');
    Route::get('admin/instructor/details/{id}',  'Admin\InstructorController@details');
    Route::post('admin/instructor/details/submit', 'Admin\InstructorController@submitInstructorDetails');

    //location Routes
    Route::get('admin/location/list', 'Admin\LocationController@locationList');
    Route::get('admin/location/details/{id}',  'Admin\LocationController@details');
    Route::get('admin/location/details',  'Admin\LocationController@details');
    Route::post('admin/location/details/submit',  'Admin\LocationController@submitLocationDetails');

    //enuiry routes

    Route::group(['middleware' => ['role:sales manager|sales person|admin']], function () {
        Route::get('admin/lead/list', 'Admin\LeadController@leadList');
        Route::get('admin/my/lead/list', 'Admin\LeadController@myLeadList');
    });

    Route::group(['middleware' => ['role:sales manager|admin']], function () {
        Route::get('admin/assigned/lead/list', 'Admin\LeadController@assignedList');
    });
    Route::group(['middleware' => ['role:sales manager|account manager|admin']], function () {
        //payment
        Route::get('admin/converted/lead/list', 'Admin\LeadController@convertedLeads');
        Route::get('admin/lead/payment/details/{leadId}', 'Admin\LeadController@paymentDetails');
        Route::post('admin/lead/payment/details/submit', 'Admin\LeadController@paymentDetailsSubmit');
    });

    Route::get('admin/lead/delete/{id}',  'Admin\LeadController@deleteLead');
    Route::get('admin/lead/details/{id}',  'Admin\LeadController@details');
    Route::get('admin/lead/details',  'Admin\LeadController@details');
    Route::post('admin/lead/details/submit',  'Admin\LeadController@submitLeadDetails');
    Route::post('admin/lead/assign',  'Admin\LeadController@assignLead');
    Route::post('admin/lead/followed_up',  'Admin\LeadController@followedUp');
    Route::post('admin/lead/mark/interest',  'Admin\LeadController@markInterest');
    Route::post('admin/lead/mark/converted',  'Admin\LeadController@markConversionStatus');


    //Route::get('admin/payment/list', 'Admin\LeadController@paymentList');


    //event Venue
    Route::get('admin/venue/list',  'Admin\EventVenueController@venueList');
    Route::get('admin/venue/delete/{id}',  'Admin\EventVenueController@deleteVenue');
    Route::get('admin/venue/details',  'Admin\EventVenueController@details');
    Route::get('admin/venue/details/{id}',  'Admin\EventVenueController@details');
    Route::post('admin/venue/details/submit', 'Admin\EventVenueController@submitVenueDetails');

    //event
    Route::get('admin/event/list', 'Admin\EventController@eventList');
    Route::get('admin/event/details/{id}', 'Admin\EventController@eventDetails');
    Route::post('admin/event/assign/venue', 'Admin\EventController@assignVenue');
    Route::post('admin/event/assign/instructor', 'Admin\EventController@assignInstructor');




});



Route::get('locations/byCountry/{countryId}', 'Controller@fetchLocations');

Route::get('get/comments', 'CommentController@getComments');
Route::get('add/comment', 'CommentController@addComment');

// 404 Redirect ---------------------
Route::get( '{url}', 'ErrorController@index')->middleware('url.redirects');
Route::get( '{url}/{url1}', 'ErrorController@index')->middleware('url.redirects');
Route::get( '{url}/{url1}/{url2}', 'ErrorController@index')->middleware('url.redirects');
Route::get( '{url}/{url1}/{url2}/{url3}', 'ErrorController@index')->middleware('url.redirects');
Route::get( '{url}/{url1}/{url2}/{url3}/{url4}', 'ErrorController@index')->middleware('url.redirects');
Route::get( '{url}/{url1}/{url2}/{url3}/{url4}/{url5}', 'ErrorController@index')->middleware('url.redirects');
